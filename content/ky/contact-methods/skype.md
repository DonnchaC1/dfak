---
layout: page
title: Skype
author: mfc
language: ky
summary: Байланыш жолдору
date: 2018-09
permalink: /ky/contact-methods/skype.md
parent: /ky/
published: true
---

Сиздин билдирүүңүздүн мазмуну да, аны кайсы бир уюмга жөнөткөнүңүз тууралуу мамлекеттик органдарга, укук коргоо органдарына белгилүү болуп калышы мүмкүн.
