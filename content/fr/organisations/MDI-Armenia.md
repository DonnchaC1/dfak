---
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English, Français
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: 24/7, GMT+4
response_time: 3 heures
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: oui
---

Media Diversity Institute - Armenia est une organisation non gouvernementale à but non lucratif qui cherche à exploiter le pouvoir des médias traditionnels, des médias sociaux et des nouvelles technologies pour sauvegarder les droits de l'homme, aider à construire une société démocratique, donner une voix à ceux qui n'en ont pas et approfondir la compréhension collective des différents types de diversité sociale.

MDI Armenia est affilié au Media Diversity Institute, basé à Londres, mais est une entité indépendante.
