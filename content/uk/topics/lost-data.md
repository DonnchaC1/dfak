---
layout: page
title: "Я втратив(-ла) свої дані"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: uk
summary: "Що робити, якщо ви втратили деякі дані"
date: 2023-04
permalink: /uk/topics/lost-data
parent: Home
---

# Я втратив(-ла) свої дані

Цифрові дані можуть бути дуже недовговічними та нестабільними і є багато способів їх втратити. Фізичне пошкодження ваших пристроїв, припинення дії облікових записів, помилкове видалення, оновлення та збої в роботі програмного забезпечення можуть пришвидшити втрату даних. Окрім того, іноді ви можете не знати, як або коли працює ваша система резервного копіювання, або просто забути облікові записи чи спосіб знаходження чи відновлення своїх даних. 

Ця стаття Комплекту екстреної цифрової допомоги допоможе вам зробити базові кроки для визначення того, як ви могли втратити дані, або знаходження потенційних стратегій для їх відновлення.

*У цьому розділі ми зосереджуємо увагу на даних, які знаходяться на пристроях. Якщо вам потрібно більше інформації про онлайн контент та облікові записи, ознайомтесь зі статтею [Комплекту екстреної цифрової допомоги щодо облікових записів](../account-access-issues).*

Надайте відповіді на запитання опитувальника, аби виявити природу вашої проблеми та знайти можливі рішення.

## Workflow

### entry-point

> У цьому розділі ми зосереджуємо увагу на даних, які знаходяться на пристроях. Якщо вам потрібно більше інформації про онлайн контент та облікові записи, ми будемо переспрямовувати вас на інші розділи Комплекту екстреної цифрової допомоги.
>
> Під пристроями ми маємо на увазі комп'ютери, мобільні пристрої, зовнішні жорсткі диски, USB-накопичувачі та SD карти.

Які типи даних ви втратили?

- [Онлайн контент](../../../account-access-issues)
- [Облікові дані](../../../account-access-issues)
- [Контент на пристрої](#content-on-device)

### content-on-device

> Часто "втрачені" дані - це дані, які було просто видалено навмисно або випадково. Перевірте "Корзину" на своєму ПК. На Android пристроях ви знайдете загублені дані в папці LOST.DIR. Якщо зниклі або втрачені файли не можна знайти в "Корзині" на вашій операційній системі, можливо, їх не було видалено, а вони можуть бути в іншому неочікуваному місці. Скористайтесь функцією пошуку в операційній системі, аби їх знайти.
>
> Спробуйте здійснити пошук за повною назвою вашого втраченого файлу. Якщо це не спрацювало або ви точно не знаєте повного імені, використайте пошук із зірочками (wildcard) (тобто якщо ви загубили файл docx, але не впевнені в назві, здійсніть пошук `*.docx`), що покаже лише файли з розширенням `.docx`. Сортуйте результати по *Даті внесення змін*, аби швидко знайти файли, якими користувались нещодавно, використовуючи опцію пошуку із зірочками.
>
> На додаток до пошуку ваших загублених даних на вашому пристрої запитайте себе, чи зробили ви резервну копію даних або чи відправляли ці дані електронною поштою або ділились ними з кимось (включно із собою ж), або ж чи додавали ці дані до свого хмарного сховища. У такому разі виконайте пошук там і, можливо, знайдете їх або ж якусь версію цих даних.
>
> Для підвищення шансів відновлення даних негайно припиніть редагування або додавання інформації до своїх пристроїв. За можливості під'єднайте диск до зовнішнього пристрою (наприклад, через USB) до окремого, безпечного комп'ютера для пошуку та відновлення інформації. Запис на диск (наприклад, завантаження або створення нових документів) може знизити шанси віднайти або відновити загублені дані.
>
> Перевірте також приховані файли та папки, адже дані, які ви загубили, можуть бути там. Ось так ви зможете це зробити на [Mac](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) та [Windows](https://support.microsoft.com/en-us/windows/view-hidden-files-and-folders-in-windows-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Для Linux - перейдіть до своєї домашньої директорії у менеджері файлів та натисніть Ctrl + H.

Як ви втратили свої дані?

- [Пристрій, де було збережено дані, зазнав фізичного пошкодження](#tech-assistance_end)
- [Пристрій, де було збережено дані, було викрадено\втрачено](#device-lost-theft_end)
- [Дані було видалено](#where-is-data)
- [Дані втрачено після оновлення програмного забезпечення](#software-update)
- [Системний або програмний інструмент зазнав збою в роботі і дані зникли](#where-is-data)


### software-update

> Іноді, коли ви встановлюєте оновлення або нові версії програмного інструменту або всієї операційної системи, можуть виникнути неочікувані проблеми і призвести до того, що система або ПЗ перестає працювати належним чином або може проявитись нестійка поведінка, включно з пошкодженням даних. Якщо ви помітили втрату даних одразу після оновлення програми або системи, розгляньте можливість повернення до попереднього стану системи. Ролбеки, повернення до попереднього стану системи, можуть бути корисними, адже вони створені, щоб ваше ПЗ або база даних були відновлені до "чистого" та стабільного стану системи навіть після помилкових операцій або перебоїв у роботі ПЗ.
>
> Метод повернення до попереднього стану або попередньої версії ПЗ залежить від побудови ПЗ. У деяких випадках це можна зробити просто, в інших - навпаки, а в деяких випадках знадобляться обхідні рішення. Таким чином, здійсніть пошук онлайн щодо відновлення до попередніх версій для цього конкретного ПЗ. Будь ласка, зверніть увагу, що відновлення до попередньої версії також відоме як даунгрейдінг ("downgrading"), тому здійсніть пошук і за цим словом. Для таких операційних систем, як Mac та Windows, передбачені власні методи даунгрейдінгу до попередніх версій. 
>
> - Для Mac систем, відвідайте [Mac Support Center knowledge base](https://support.apple.com) та використайте ключове слово "downgrade" для своєї версії macOS, прочитайте більше та знайдіть інструкції.
> - Для Windows систем, процедури є різними залежно від того, що ви хочете зробити: видалити окреме оновлення або відновити всю систему. В обох випадках ви можете знайти інструкції на [Microsoft Support Center](https://support.microsoft.com), наприклад, для Windows 11 ознайомтесь зі статтею "Як видалити встановлене оновлення" на цій [FAQ сторінці](https://support.microsoft.com/en-us/help/12373/windows-update-faq).

Чи було успішним повернення ПЗ до попереднього стану?

- [Так](#resolved_end)
- [Ні](#where-is-data)


### where-is-data

> Регулярні резервні копіювання даних є хорошою і рекомендованою практикою. Іноді ми забуваємо про налаштоване автоматичне резервне копіювання, тому перевірте, чи включено таку опцію на ваших пристроях і використовуйте резервні копії для відновлення своїх даних. Якщо ви зараз не робите резервні копії, рекомендовано планувати майбутнє резервування, а також ви можете знайти більше порад щодо того, як налаштувати резервне копіювання, в [кінцевих порадах цієї статті](#resolved_end).

Аби перевірити, чи є у вас резервні копії даних, які ви втратили, спочатку запитайте себе, де загублені вами дані зберігались.

- [На пристроях зберігання даних (зовнішні диски, USB диски, SD карти)](#storage-devices_end)
- [На комп'ютері](#computer)
- [На мобільному пристрої](#mobile)

### computer

Яку операційну систему встановлено на вашому комп'ютері?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Аби перевірити, чи на вашому MacOS пристрої включено опцію резервного копіювання, і використати цю копію для відновлення ваших даних, перевірте опції вашого [iCloud](https://support.apple.com/en-us/HT208682) або [Time Machine](https://support.apple.com/en-za/HT201250), щоб пошукати резервну копію.
>
> Ще одне місце для пошуку - це перелік "Останніх програми" (Recent Items), який реєструє додатки, файли та сервери, якими ви користувались під час кількох останніх сесій на комп'ютері. Щоб знайти файл та повторно його відкрити, перейдіть в Apple меню у верхньому лівому куті, оберіть "Останні програми" (Recent Items) і перегляньте перелік файлів. Більше інформації можна знайти в [офіційному керівництві користувача macOS](https://support.apple.com/en-za/guide/mac-help/mchlp2305/mac).

Чи вдалося вам знайти свої дані або відновити їх?

- [Так](#resolved_end)
- [Ні](#macos-restore)

### macos-restore

> У деяких випадках є безкоштовні інструменти та інструменти з відкритим вихідним кодом, які можуть допомогти знайти загублений контент і відновити його. Іноді вони обмежені в користуванні і більшість відомих інструментів є платною. 
>
> Для прикладу ви можете спробувати [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) або [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Чи була ця інформація корисною для відновлення ваших даних (частково або повністю)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)

### windows-computer

> Щоб перевірити, чи включено у вас резервне копіювання на вашій Windows машині, подивіться [Microsoft Support: Резервне копіювання та відновлення у Windows ](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
> Windows включає опцію **Часова шкала**, яка передбачена для підвищення продуктивності шляхом збереження записів про файли, які ви використовували, сайтів, які відвідували, та інших дій, які ви виконували на своєму комп'ютері. Якщо ви не можете згадати, де зберегли документ, клікніть на іконці Часова шкала в панелі задач Windows 10 для того, щоб побачити візуалізований лог, організований по даті, та перейдіть на потрібний вам пункт, клікнувши на відповідній іконці попереднього перегляду. Це може допомогти вам знайти файл, який було переіменовано. Якщо Часову шкалу включено, деяка активність вашого ПК, наприклад, файли, які ви редагували в Microsoft Office, може також синхронізуватись із вашим мобільним пристроєм або іншим комп'ютером, яким ви користуєтесь, отже ви можете знайти резервну копію своїх загублених даних на іншому пристрої. Прочитайте більше про Часову шкалу і як нею користуватись [в офіційному керівництві](https://support.microsoft.com/en-us/windows/get-help-with-timeline-febc28db-034c-d2b0-3bbe-79aa0c501039).

Чи вдалося вам знайти свої дані та відновити їх?

- [Так](#resolved_end)
- [Ні](#windows-restore)

### windows-restore

> У деяких випадках є безкоштовні інструменти та інструменти з відкритим вихідним кодом, які можуть допомогти знайти загублений контент і відновити його. Іноді вони обмежені в користуванні і більшість відомих інструментів є платною.
>
> Для прикладу ви можете спробувати [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (див. [покрокову інструкцію](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/data-recovery-software.html) або [Recuva](http://www.ccleaner.com/recuva).

Чи була ця інформація корисною для відновлення ваших даних (повного або часткового)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)


### linux-computer

> Деякі популярні Linux дистрибутиви, зокрема, Ubuntu, мають вбудований інструмент для резервного копіювання, наприклад, [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) для Ubuntu. Якщо вбудований інструмент для резервного копіювання включено до комплекту вашої операційної системи, можливо, ви отримували повідомлення про вмикання автоматичного резервного копіювання під час першого запуску вашого комп'ютера. Пошукайте ваш Linux дистрибутив і перевірте, чи він включає вбудований інструмент для резервного копіювання, яка процедура перевірки, чи ввімкнено резервне копіювання і як відновити дані з резервних копій.
>
> Прочитайте [Ubuntu & Deja Dup - Get up, backup](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) для перевірки, чи налаштовано на вашому ПК автоматичне резервне копіювання. Прочитайте ["Full System Restore with Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) для отримання інструкцій, як відновити втрачені дані з існуючої резервної копії.

Чи була ця інформація корисною для відновлення ваших даних (повного або часткового)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)

### mobile

Яка операційна система на вашому мобільному пристрої?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Можливо, ви ввімкнули автоматичне резервне копіювання на iCloud або iTunes. Прочитайте ["Restore or set up your device from an iCloud backup"](https://support.apple.com/en-us/HT204184) для перевірки, чи є у вас доступні резервні копії, та дізнайтесь, як відновити свої дані.

Чи вдалось вам знайти свої резервні копії та відновити свої дані?

- [Так](#resolved_end)
- [Ні](#phone-which-data)

###  phone-which-data

Що з переліченого можна застосувати до даних, які ви втратили?

- [Дані, згенеровані додатками, наприклад, контакти, стрічки тощо](#app-data-phone)
- [Дані, згенеровані користувачем, наприклад, фото, відео, аудіо, примітки](#ugd-phone)

### app-data-phone

> Мобільний додаток - це прикладне програмне забезпечення, створене для запуску на вашому мобільному пристрої. Однак, до більшої частини цих додатків можна також отримати доступ через настільний вебоглядач вашого ПК. Якщо ви зіштовхнулись із втратою даних, згенерованих додатками на ваших мобільних пристроях, спробуйте отримати доступ до цих даних через вебоглядач вашого ПК шляхом доступу через вебінтерфейс за допомогою ваших облікових даних для цього додатку. Ви можете знайти свої втрачені дані через інтерфейс вебоглядача.

Чи була інформація корисною для відновлення ваших даних (повного або часткового)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)

### ugd-phone

> Дані, згенеровані користувачем, - це дані, які ви створили або згенерували через певний додаток, і у випадку втрати даних ви можете перевірити, чи цей додаток має налаштування резервного копіювання, встановлені за замовчуванням, або чи ввімкнено способи для їх відновлення. Приміром, якщо ви користуєтесь WhatsApp на своєму мобільному пристрої і зникли розмови або стались якісь помилки, ви можете відновити свої розмови, якщо ви ввімкнули налаштування відновлення в WhatsApp, або якщо ви користуєтесь додатком для створення та ведення нотаток із чутливою або персональною інформацією, також іноді можливо, що опцію резервного копіювання ввімкнено без вашої участі.
>
> Більшість даних, згенерованих користувачами в додатках і збережених на телефоні, буде скопійовано до вашої резервної копії, або в хмару, або на локальний диск. Див. поради в кінці цієї статті з інструкціями, як створити резервну копію вашого пристрою.

Чи була інформація корисною для відновлення ваших даних (повного або часткового)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)

### android-phone

> У Google є вбудований сервіс в Android, який має назву Android Backup Service. За замовчуванням цей сервіс робить резервні копії декількох типів даних та асоціює їх із відповідними Google сервісами, до яких ви також можете отримати доступ через веб. На більшості пристроїв можна ввімкнути налаштування синхронізації, для цього перейдіть у Налаштування > Облікові записи > Google, потім оберіть свою Gmail адресу. Якщо ви втратили дані того типу, який синхронізується з вашим обліковим записом Google, можливо, ви зможете відновити дані, зайшовши до свого облікового запису Google через вебоглядач.  

Чи була інформація корисною для відновлення ваших даних (повного або часткового)?

- [Так](#resolved_end)
- [Ні](#tech-assistance_end)


### tech-assistance_end

> Якщо втрату даних спричинило фізичне пошкодження, наприклад, ваші пристрої впали на підлогу або в воду, зазнали перепадів напруги або інших пошкоджень, найбільш можлива ситуація в тому, що вам необхідно провести відновлення даних, збережених на вашому пристрої. Якщо ви не знаєте, як виконати ці дії, зверніться до спеціаліста ІТ, який має навички роботи з обладнанням та апаратним забезпеченням і зможе вам допомогти. Однак, в залежності від контексту та чутливості даних, які необхідно відновити, залучення ІТ компанії може бути не рекомендовано, краще надати перевагу ІТ спеціалістам, яких ви знаєте і яким довіряєте.


### device-lost-theft_end

> Якщо пристрої було втрачено, викрадено або вилучено, одразу змініть всі свої паролі і ознайомтесь із нашою статтею [якщо ваш пристрій було втрачено](../../../lost-device).
>
> Якщо ви є активістом громадянського суспільства і вам необхідна допомога з отриманням пристрою на заміну втраченого, зверніться до організацій, зазначених нижче.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Пам'ятайте: для відновлення втрачених даних дуже важливо не втрачати час. Наприклад, відновлення файлу, який ви випадково видалили кілька годин або день тому, може бути вірогіднішим, ніж файлу, який ви втратили кілька місяців тому.
>
> Розгляньте можливість використання інструменту для відновлення даних, які ви втратили (через видалення або пошкодження), такого як [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (для Windows, Mac або Linux - див [покрокові інструкції](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (для Windows, Mac, Android або iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (для Windows або Mac), або [Recuva](http://www.ccleaner.com/recuva) (для Windows). Зверніть увагу, що ці інструменти не завжди працюють, тому що ваша операційна система могла перезаписати нові дані поверх вашої видаленої інформації. Зважаючи на це, вам необхідно по мінімуму користуватися своїм комп'ютером у період між видаленням файлу і спробою відновити його одним із інструментів, зазначених вище.
>
> Для підвищення шансів на відновлення даних одразу перестаньте користуватись своїми пристроями. Постійне записування на жорсткий диск може знизити шанси знаходження та відновлення даних.
>
> Якщо нічого з наведеного не спрацювало для вас, велика ймовірність того, що вам необхідно провести відновлення даних, збережених на вашому жорсткому диску. Якщо ви не знаєте, як виконати ці дії, зверніться до спеціаліста ІТ, який має навички роботи з обладнанням та апаратним забезпеченням і зможе вам допомогти. Однак, в залежності від контексту та чутливості даних, які необхідно відновити, залучення ІТ компанії може бути не рекомендовано, краще надати перевагу ІТ спеціалістам, яких ви знаєте і яким довіряєте.

### resolved_end

Сподіваємось, що ця стаття була корисною. Будь ласка, надсилайте свої відгуки [електронною поштою](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Резервне копіювання - це завжди хороша ідея. Можна мати багато різних резервних копій і зберігати їх в іншому місці, а не зі своїми оригінальними даними! Залежно від ситуації, намагайтесь зберігати свої дані в хмарних сервісах і на фізичних зовнішніх пристроях, які слід тримати відключеними від комп'ютера під час під'єднання до інтернету.
- Для обох типів резервних копій використовуйте шифрування для захисту даних. Проводьте регулярні та інкрементальні резервні копіювання своїх найважливіших даних, перевіряйте їхню готовність і пробуйте їх відновити до оновлення програм або операційних систем.
- Зробіть структуровану систему папок - кожна людина має власний спосіб організації важливих даних та інформації, не буває так, щоб усе підходило всім. Тим не менш розгляньте можливість створення системи папок, яка підходить саме вам. Створюючи послідовну систему папок, ви можете спростити собі життя, розуміючи, резервну копію якої папки або файлів слід зробити, наприклад, папки із важливою інформацією, над якою ви працюєте, де ви повинні зберігати особисту або чутливу інформацію про себе та ваших співробітників, тощо. Глибоко вдихніть і виділіть трохи часу на планування видів даних, які ви створюєте або якими керуєте, продумайте систему папок, яка буде послідовною і допоможе навести лад.

#### resources

- [Access Now Helpline Community Documentation: Secure back up](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: Back-up Tactics](https://securityinabox.org/en/files/backup/)
- [Official page on Mac backups](https://support.apple.com/mac-backup)
- [How to back up data regularly on Windows](https://support.microsoft.com/en-us/windows/backup-and-restore-in-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef)
- [How to enable regular backups on iPhone](https://support.apple.com/en-us/HT203977)
- [How to enable regular backups on Android](https://support.google.com/android/answer/2819582?hl=en)
- [Securily backup your data](https://johnopdenakker.com/securily-backup-your-data/)
- [How to Back Up Your Digital Life](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Data recovery](https://en.wikipedia.org/wiki/Data_recovery)
