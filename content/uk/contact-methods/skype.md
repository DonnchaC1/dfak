---
layout: page
title: Skype
author: mfc
language: uk
summary: Способи зв'язку
date: 2018-09
permalink: /uk/contact-methods/skype.md
parent: /uk/
published: true
---

Зміст вашого повідомлення та факт вашого спілкування з організаціїєю можуть стати відомими органам державної влади чи правоохоронним органам.