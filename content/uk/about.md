---
layout: page.pug
title: "About"
language: uk
summary: "Про комплект екстреної цифрової допомоги"
date: 2023-05
permalink: /uk/about/
parent: Home
---

Комплект екстреної цифрової допомоги - це результат спільних зусиль [RaReNet (Rapid Response Network)](https://www.rarenet.org/) та [CiviCERT](https://www.civicert.org/).

<iframe src="https://archive.org/embed/dfak-tech-demo" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Мережа швидкого реагування - це глобальна мережа організацій швидкого реагування та активістів у сфері цифрової безпеки, включно з Access Now, Amnesty Tech, Center for Digital Resilience, CIRCL, EFF, Freedom House, Front Line Defenders, Global Voices, Greenhost, Hivos & the Digital Defenders Partnership, Internews, La Labomedia, Open Technology Fund, Virtualroad. Членами цієї мережі є також окремі експерти з питань безпеки, які працюють у сфері цифрової безпеки та швидкого реагування.

Деякі із цих організацій та осіб входять до CiviCERT, глобальної мережі служб підтримки та провайдерів інфраструктури цифрової безпеки, які в основному зосереджені на тому, щоб надавати підтримку особам та групам осіб, що прагнуть до соціальної справедливості та захисту прав людини і цифрових прав. CiviCERT - це професійна структура для різноманітних ініціатив спільноти CERT (Комп'ютерна група реагування на надзвичайні ситуації) щодо швидкого реагування. CiviCERT має акредитацію від Trusted Introducer, Європейської мережі комп'ютерних груп реагування на надзвичайні ситуації.

Також Комплект екстреної цифрової допомоги - це [проєкт із відкритим вихідним кодом, який приймає зовнішню допомогу].(https://gitlab.com/rarenet/dfak).

Завдяки [Metamorphosis Foundation](https://metamorphosis.org.mk) для [Albanian localization](https://digitalfirstaid.org/sq/) та [EngageMedia](https://engagemedia.org/) для [Burmese](https://digitalfirstaid.org/my/), [Indonesian](https://digitalfirstaid.org/id/) і [Thai](https://digitalfirstaid.org/th/) є локалізація Комплекту екстреної цифрової допомоги.

Якщо ви хочете використовувати Комплект екстреної цифрової допомоги в умовах з обмеженим або ускладненим підключенням до інтернету, завантажте [офлайнову версію](https://digitalfirstaid.org/dfak-offline.zip).

Якщо у вас виникли будь-які коментарі, пропозиції чи запитання щодо Комплекту екстреної цифрової допомоги, пишіть нам на адресу: dfak@digitaldefenders.org

Відбиток GPG: 1759 8496 25C1 56EC 1EB4 1F06 6CC1 888F 5D75 706B
