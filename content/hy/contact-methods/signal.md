---
layout: page
title: Signal
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2018-09
permalink: /hy/contact-methods/signal.md
parent: /hy/
published: true
---

Signal-ը երաշխավորում է, որ նամակագրությունը տեսանելի է միայն ձեր և հասցեատիրոջ համար։ Ինչպես նաև, որ ուրիշ ոչ ոք չի իմանա ձեր մեջ եղած նամակագրության մասին։ Signal-ը օգտագործում է ձեր հեռախոսի համարը որպես մուտքանուն։ Հետևաբար կազմակերպության հետ Signal-ով կապ հաստատելիս, դուք ավտոմատ տրամադրում եք նրանց ձեր հեռախոսահամարը։

Ռեսուրսներ. [Ինչպես օգտագործել Signal՝ Android-ով](https://ssd.eff.org/en/module/how-use-signal-android), [Ինչպես օգտագործել Signal՝ iOS-ով](https://ssd.eff.org/en/module/how-use-signal-ios), [Ինչպես օգտագործել Signal առանց հեռախոսահամար տրամադրելու](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
