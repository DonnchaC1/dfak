---
layout: sidebar.pug
title: "Dikush që e njoh është arrestuar"
author: Peter Steudtner, Shakeeb Al-Jabri, Metamorphosis Foundation
language: sq
summary: "Një mik, koleg apo anëtar i familjes është arrestuar nga forcat e sigurisë. Dëshironi ta kufizoni ndikimin e arrestimit të tyre mbi ta dhe këdo tjetër që mund të jetë përzier me ta."
date: 2019-03-13
permalink: /sq/arrested/
parent: Home
sidebar: >
	<h3>Lexoni më shumë se çfarë të bëni nëse dikush që ju e njihni është arrestuar:</h3>

	 <ul>
  <li><a href="https://www.newtactics.org/search/solr/arrest">Marrja e mbështetjes për zhvillimin e fushatës në emër të personit të arrestuar</a></li>
	</ul>

---

# Dikush që unë e njoh është arrestuar

Arrestimet e mbrojtësve të të drejtave të njeriut, gazetarëve dhe aktivistëve i vendosin ata dhe personat me të cilët punojnë dhe jetojnë në një rrezik të madh.

Ky udhëzues është i orientuar posaçërisht për vendet që kanë mungesë të respektimit të të drejtave të njeriut dhe procesit të rregullt ligjor, ose ku autoritetet mund të anashkalojnë procedurat ligjore, ose ku aktorët jo-shtetërorë veprojnë lirshëm, paraburgimet ose arrestimet paraqesin një rrezik më të madh për viktimat, kolegët dhe të afërmit e tyre.

Në këtë udhëzues synojmë të lehtësojmë rrezikun me të cilin përballen njerëzit e arrestuar dhe të kufizojmë mundësinë e qasjes nga ana e autoriteteve arrestuese në të dhënat e ndjeshme që mund të inkriminojnë viktimat dhe kolegët e tyre, ose që mund të përdoren për të komprometuar operacionet e tjera.

Kujdesi për të arrestuarin si dhe për ndikimet digjitale të këtij paraburgimi mund të jetë rraskapitës dhe sfidues. Mundohuni të gjeni njerëz të tjerë që t'ju mbështesin dhe të koordinoni veprimet tuaja me komunitetin e përfshirë.

Gjithashtu është e rëndësishme që të kujdeseni për veten tuaj dhe të tjerët që janë të ndikuar nga ky arrestim, duke:

- [u kujdesur për mirëqenien dhe nevojat tuaja psiko-sociale](../self-care),
- duke u kujdesur për anën ligjore të punës suaj mbështetëse - mund të gjeni organizata që ofrojnë mbështetje ligjore në [faqen e mbështetjes së DFAK](../support)
- [marrja e mbështetjes për zhvillimin e fushatës në emër të personit të arrestuar](https://www.newtactics.org/search/solr/arrest)

Nëse ndiheni të mbingarkuar teknikisht ose emocionalisht ose (për çfarëdo arsye) nuk jeni në gjendje të ndiqni hapat e përshkruar më poshtë, ju lutemi kërkoni ndihmë dhe udhëzime nga organizatat e renditura [këtu](../support) që ofrojnë përzgjedhje fillestare midis shërbimeve të tyre.


## Bëni një plan

Para se të veproni në seksionet e ndryshme që i përshkruajmë më poshtë, ju lutemi kaloni hapat e mëposhtëm:

- Mundohuni ta lexoni të gjithë udhëzuesin dhe të merrni një pasqyrë të të gjitha fushave të rëndësishme të ndikimit përpara se të merrni masa në aspektet e veçanta. Arsyeja është se seksionet e ndryshme nxjerrin në pah skenarë të ndryshëm të kërcënimeve të cilat mund të përputhen, kështu që do t'ju duhet të ndërtoni sekuencën tuaj të veprimeve.
- Ndani kohë me miqtë ose me ekipin për të kryer vlerësime të ndryshme të rrezikut që janë të nevojshme në seksione të ndryshme.

<a name="harm-reduction"></a>
## Bëhuni gati para se të veproni

A keni arsye të besoni se ky arrestim ose paraburgim mund të çojë në pasoja për familjarët, miqtë, ose kolegët e të arrestuarit, duke përfshirë edhe veten tuaj?

Ky udhëzues do t'ju njoftojë me një seri hapash për të ndihmuar në ofrimin e zgjidhjeve që mund të ndihmojnë në zvogëlimin e ekspozimit të të arrestuarit dhe të kujtdo tjetër që është i përfshirë me ta.

### Këshilla për përgjigje të koordinuar

Në të gjitha situatat kur një person është arrestuar, gjëja e parë që duhet të theksohet është se shpesh kur ndodhin incidente të tilla, disa miq ose kolegë reagojnë menjëherë, duke rezultuar ose në përpjekje të dyfishta ose kontradiktore. Pra, është e rëndësishme të mbani mend se koordinimi dhe veprimi i bashkërenduar, në nivelin lokal dhe ndërkombëtar, janë të domosdoshëm si për të mbështetur të arrestuarin dhe për t'u kujdesur për të gjithë të tjerët brenda rrjeteve të tyre të mbështetjes, familjes dhe miqve.

- Krijoni një ekip krize që do të koordinojë të gjitha aktivitetet e mbështetjes, kujdesit, fushatave, etj.
- Përfshini anëtarët e familjes, partnerët, etj., sa më shumë që të jetë e mundur (duke respektuar kufijtë e tyre nëse, për shembull, ata janë të dërrmuar).
- Vendosni qëllime të qarta për fushatën tuaj mbështetëse (dhe shqyrtoni ato shpesh): për shembull, ju mund të dëshironi të arrini lirimin e të arrestuarit, të siguroni mirëqenien e tyre, ose të mbroni familjen dhe përkrahësit dhe të siguroni mirëqenien e tyre si qëllime tuaja të menjëhershme.
- Pajtohuni për kanale të sigurta të komunikimit dhe shpeshtësinë e komunikimit, si dhe kufizimet (p.sh. asnjë komunikim ndërmjet orës 10 të mbrëmjes dhe orës 8 të mëngjesit, përveç rasteve të urgjencave të reja ose lajmeve shumë të rëndësishme).
- Shpërndani detyrat midis anëtarëve të ekipit dhe kontaktoni palët e treta për mbështetje (analiza, avokim, punë me mediat, dokumentacionin, etj.).
- Kërkoni mbështetje të mëtejshme jashtë "ekipit të krizës" për përmbushjen e nevojave themelore (p.sh. ushqim i rregullt, etj.)

### Masat paraprake të sigurisë digjitale

Nëse keni arsye të keni frikë nga pasojat për veten tuaj ose mbështetësit e tjerë, përpara se të trajtoni ndonjë emergjencë digjitale të lidhur me arrestimin e mikut tuaj, është gjithashtu e rëndësishme të ndërmerrni disa hapa parandalues në nivelin e sigurisë digjitale për të mbrojtur veten tuaj dhe të tjerët nga rreziku i menjëhershëm.

- Bini dakord se cilat kanale komunikimi (të sigurta) do të përdorë rrjeti juaj i mbështetësve për të koordinuar masat zbutëse dhe për të komunikuar në lidhje me të paraburgosurin dhe pasojat e mëtejshme. Ndani të drejtat e administrimit me përdorues të shumtë në grupet e mesazheve ose mjete të tjera të komunikimit në grup që përdorni.
Mund të merrni në konsideratë aktivizimin e mesazheve që zhduken për siguri shtesë.
- Zvogëloni të dhënat në pajisjet tuaja në minimumin e nevojshëm, dhe mbroni të dhënat në pajisjet tuaja me [enkriptim](https://ssd.eff.org/en/module/keeping-your-data-safe#1).
- Krijoni [kopje rezervë të sigurt, të enkriptuar](https://communitydocs.accessnow.org/182-Secure_Backup.html) të të gjitha të dhënave tuaja dhe mbajini ato në një vend i cili nuk mund të gjendet gjatë bastisjeve ose arrestimeve të mëtejshme.
- Ndani fjalëkalimet për pajisjet, llogaritë në internet etj. me persona të besuar që nuk janë në rrezik të menjëhershëm. [Softuerët për menaxhimin e fjalëkalimeve](https://securityinabox.org/en/passwords/passwords-and-2fa/#use-a-password-manager) mund t'ju ndihmojnë ta bëni këtë në mënyrë të sigurt.
- Bini dakord për veprimet që duhen ndërmarrë (si pezullimi i llogarisë, fshirja e pajisjes në distancë, etj.) si reagim i parë ndaj çdo arrestimi të mëtejshëm.


## Vlerësimi i rrezikut
### Zvogëloni dëmin e mundshëm të shkaktuar nga veprimet tuaja

Në përgjithësi, duhet të përpiqeni t'i bazoni veprimet tuaja në këtë pyetje:

- Cilat janë ndikimet e veprimeve të vetme dhe të kombinuara mbi të arrestuarin, por edhe mbi komunitetet e tyre, bashkëaktivistët, miqtë, familjen, etj., duke përfshirë edhe veten tuaj?

Secila nga seksionet e mëposhtme do të përshkruajë aspekte të veçanta të këtij vlerësimi të rrezikut.

Konsideratat themelore janë:

- Para se të fshini llogaritë, të dhënat, temat e mediave sociale, etj., sigurohuni që keni dokumentuar përmbajtjen dhe informacionin që e fshini, veçanërisht nëse mund të keni nevojë të riktheni atë përmbajtje ose informacion, ose nëse ju duhet për dëshmi të mëvonshme.
- Nëse i ndryshoni fjalëkalimet, fshini ose hiqni llogaritë ose skedarët, kini parasysh se:
    - Autoritetet mund ta interpretojnë këtë si shkatërrim ose heqje të provave
    - Kjo mund ta vështirësojë situatën e të arrestuarit, nëse ata do të lejonin qasjen në këto llogari ose skedarë (fajlla), dhe autoritetet nuk mund t'i gjejnë ato, pasi i arrestuari do të paraqitej si jo i besueshëm dhe kjo fshirje mund të shkaktojë marrjen e veprimeve kundër tyre.
- Nëse i informoni njerëzit se informacioni i tyre personal ruhet në një pajisje ose llogari të kapur nga autoritetet, dhe ky komunikim përgjohet, kjo mund të përdoret si provë shtesë sa i përket lidhjes me të arrestuarin.
- Ndryshimet në procedurat e komunikimit (duke përfshirë fshirjen e llogarive, etj.) mund të tërheqin vëmendjen e autoriteteve.

### Informimi i kontakteve

Në përgjithësi, është e pamundur të përcaktohet nëse autoritetet që kanë bërë arrestimin kanë kapacitetin për të hartuar rrjetin e kontaktit të të paraburgosurit dhe nëse e kanë bërë këtë apo jo.
Prandaj, ne duhet të supozojmë rastin më të keq - se ata e kanë bërë atë ose se eventualisht do ta bëjnë.

Para se të filloni të informoni kontaktet e të arrestuarit, ju lutemi vlerësoni rrezikun e informimit të tyre:

- A keni një kopje të listës së kontakteve të të arrestuarit? A mund të kontrolloni se kush është në listën e kontakteve, si në pajisjet e tyre, ashtu edhe në llogaritë e tyre të e-mailit, dhe në platformat e rrjeteve sociale? Përpiloni një listë të kontakteve të mundshme për të fituar një pasqyrë se kush mund të preket. [Ruajeni](https://securityinabox.org/en/files/secure-file-storage/) dhe [transferojeni](https://securityinabox.org/en/communication/private-communication/) këtë listë në mënyrë sa më të sigurt që të jetë e mundur dhe ndajeni atë vetëm në bazë të parimit se kush "duhet ta dijë".
- A ekziston rreziku që informimi i kontakteve të mund t'i lidhë ata më afër me të arrestuarin dhe kjo mund të (keq)përdoret nga autoritetet që kanë bërë arrestimin kundër tyre?
- A duhet të informohen të gjithë, apo vetëm një grup i caktuar njerëzish në listën e kontakteve?
- Kush do të informojë, edhe atë cilat kontakte? Kush është tashmë në kontakt me kë? Cili është ndikimi i këtij vendimi?
- Vendosni kanalin e komunikimit më të sigurt, duke përfshirë takime personale në hapësira ku nuk ka mbikëqyrje nga kamerat e sigurisë (CCTV) për informimin e kontakteve të implikuara.

### Dokumentoni për të mbajtur evidencë

Për më shumë informacion se si të ruani dëshmi digjitale, shkoni te [Udhëzuesi i DFAK-së për dokumentimin e incidenteve në internet](../documentation).

Para se të fshini ndonjë përmbajtje nga ueb-faqet, faqet e mediave sociale, diskutimet etj., mund të dëshironi të siguroheni që i keni dokumentuar këto më parë. Një arsye për ruajtjen e këtij informacioni do të ishte kapja e çdo shenje ose dëshmie të llogarive të abuzuara - si përmbajtje shtesë ose imituese - ose përmbajtje që ju nevojitet si provë ligjore.

Në varësi të ueb-faqes ose platformës së mediave sociale ku dëshironi të dokumentoni burimet ose të dhënat në internet, mund të përdoren qasje të ndryshme:

- Mund të merrni pamje të ekraneve (screenshots) të pjesëve përkatëse (sigurohuni që të dhënat kohore, URL, etj. të jenë të përfshira në pamjet e ekraneve).
- Mund të kontrolloni që ueb-faqet ose blogjet përkatëse janë të indeksuara në [Wayback Machine](https://archive.org/web), ose të shkarkoni ueb-faqet ose blogjet në kompjuterin tuaj lokal.
- Mund të gjeni informacion më të plotë se si të ruani informacionin në internet në [Udhëzuesin e DFAK për dokumentim](../documentation).

*Mos harroni se është e rëndësishme të mbani informacionin që keni shkarkuar në një pajisje të sigurt e cila ruhet në një vend të sigurt.*

### Konfiskimi i pajisjes

Nëse ndonjë pajisje e personit të arrestuar është konfiskuar gjatë ose pas arrestimit, ju lutemi lexoni [Udhëzuesin e DFAK - kam humbur pajisjet e mia](../topics/lost-device), në veçanti seksionin për [fshirjen nga distanca](../topics/lost-device/questions/find-erase-device), duke përfshirë rekomandimet në rast të arrestimit.


### Të dhënat dhe llogaritë inkriminuese në internet

Nëse pajisjet e të paraburgosurit përmbajnë ose janë të lidhura me llogari online që përmbajnë informacione që mund të jenë të dëmshme për të ose për persona të tjerë, mund të jetë një ide e mirë të përpiqeni të kufizoni qasjen e të arrestuarve në këtë informacion.

Para se ta bëni këtë, krahasoni rrezikun që paraqet ky informacion me rrezikun e paraqitur nga forcat e sigurisë të cilat mund të shqetësohen për shkak të mungesës së qasjes në këtë informacion (ose ndërmarrjes së veprimeve ligjore për shkak të shkatërrimit të provave). Nëse rreziku që paraqet ky informacion është më i lartë, mund të vazhdoni të hiqni të dhënat përkatëse dhe/ose të mbyllni/pezulloni dhe hequr lidhjen mes llogarive duke i ndjekur udhëzimet më poshtë.

#### Pezullimi ose mbyllja e llogarive në internet

Nëse keni qasje në llogaritë që dëshironi të mbyllni, mund të ndiqni procese e përshkruara nga platformat e ndryshme për të mbyllur një llogari. Sigurohuni që të keni një kopje rezervë ose kopje të përmbajtjes dhe të dhënave të fshira! Kini parasysh se pas mbylljes së një llogarie përmbajtja nuk do të bëhet menjëherë e paarritshme: në rastin e Facebook-ut, për shembull, mund të duhen deri në dy javë që përmbajtja të fshihet nga të gjithë serverët.

Kini parasysh se ka opsione të ndryshme për të pezulluar ose mbyllur llogaritë, të cilat kanë ndikime të ndryshme në përmbajtje dhe qasje:

- Bllokimi: Bllokimi i një llogarie parandalon këdo që të hyjë në llogari. Kur një llogari është e bllokuar, të gjitha kredencialet dhe tokenët e aplikacionit hiqen, që do të thotë se identifikimi në llogari nuk është i mundur, por llogaria mbahet aktive dhe çdo përmbajtje publike do të jetë ende e dukshme.
- Pezullimi i përkohshëm: Kur kërkoni një pezullim të përkohshëm, platformat do ta bëjnë llogarinë të padisponueshme përkohësisht.
Kjo do të thotë se askush nuk do të jetë në gjendje të hyjë në llogari dhe llogaria do të shfaqet gjithashtu e pezulluar për këdo që përpiqet të ndërveprojë me të, duke shfaqur një mesazh "llogari e pezulluar" kur shikohet ose aksesohet.
- Mbyllja përfundimtare: I referohet mbylljes përfundimtare të llogarisë, në një mënyrë që nuk është e rikuperueshme në të ardhmen.
Ky është zakonisht mekanizmi më pak i preferuar i kontrollit, por është i vetmi i disponueshëm për disa shërbime dhe platforma. Duke pasur parasysh pamundësinë për të rikuperuar llogarinë përkatëse, është i nevojshëm pëlqimi nga përfaqësuesi ligjor ose familja përpara se të kërkohet mbyllja e llogarisë.

Nëse nuk keni qasje në llogaritë e të arrestuarit ose keni nevojë për veprim më urgjent në llogaritë e mediave sociale, ju lutemi merrni mbështetje nga organizatat e renditura [këtu](../support) që ofrojnë siguri të llogarisë mes shërbimeve të tyre.

#### Shkëputja e lidhjeve të llogarive nga pajisjet

Ndonjëherë ju gjithashtu mund të dëshironi t'i shkëputni lidhjet e llogarive  me pajisjet, pasi kjo lidhje mund të sigurojë qasje në të dhëna të ndjeshme për këdo që e kontrollon pajisjen e lidhur. Për ta bërë këtë, mund të ndiqni [këto udhëzime](../topics/lost-device/questions/accounts).

Mos harroni të shkëputni lidhjet e [llogarive bankare në internet](#online_bank_accounts) me pajisjet.


#### Ndryshoni fjalëkalimet

Nëse vendosni të mos i mbyllni ose t'i pezulloni llogaritë, mund të jetë megjithatë e dobishme të ndryshoni fjalëkalimet e tyre, duke ndjekur [këto udhëzime](../topics/lost-device/questions/passwords).

Konsideroni gjithashtu mundësinë e autentifikimit me dy faktorë për të rritur sigurinë e llogarive të të arrestuarit, duke ndjekur [këto udhëzime](../topics/lost-device/questions/2fa).

Në rast se të njëjtat fjalëkalime janë përdorur në llogari të ndryshme, duhet të ndryshoni të gjitha fjalëkalimet e prekura në ato llogari pasi ato gjithashtu mund të jenë komprometuara.

** Këshilla të tjera për ndryshimin e fjalëkalimeve **

- - Përdorni një [softuer për menaxhimin e fjalëkalimeve](https://securityinabox.org/en/passwords/passwords-and-2fa/#use-a-password-manager) (si [KeepassXC](https://keepassxc.org)) për të dokumentuar fjalëkalimet e ndryshuara për përdorim të mëtejshëm ose që t'ia dorëzoni të arrestuarit pas lirimit.
- Sigurohuni t'i tregoni të arrestuarit, më së voni pas lirimit të tyre, për fjalëkalimet e ndryshuara dhe kthejuani pronësinë e llogarive.


#### Hiqni anëtarësinë në grupe dhe hiqni dosjet e përbashkëta

Nëse i arrestuari është në ndonjë grup (si për shembull por pa u kufizuar në)  Facebook, WhatsApp, Signal, ose biseda në grup në Wire, ose mund të hyjë në dosje të përbashkëta në internet, dhe prania e saj në këto grupe u jep arrestuesve qasje në informacion të privilegjuar dhe potencialisht të rrezikshëm, ndoshta do të dëshironi të hiqni atë nga grupet ose hapësirat e përbashkëta në internet.

**Udhëzime se si të hiqni anëtarësinë nga grupet e shërbime të ndryshme të porosive të çastit:**

- [WhatsApp](https://faq.whatsapp.com/en/android/26000116/?category=5245251)
- Telegram
    - Në iOS: Personi që e ka krijuar grupin mund t'i largojë pjesëmarrësit duke zgjedhur "Informacione për grupin" (Group info),  dhe duke e lëvizur përdoruesin që duan të largojnë në të majtë.
    - në Android: prekni ikonën e lapsit. Zgjidhni "Anëtarët" (Members) dhe prekni tre pikat pranë emrit të anëtarit që dëshironi ta hiqni dhe zgjidhni "Hiqe nga grupi" (Remove from group).
- [Wire](https://support.wire.com/hc/en-us/articles/203526400-How-can-I-remove-someone-from-a-group-conversation-)
- [Signal](https://support.signal.org/hc/en-us/articles/360050427692-Manage-a-group#remove)


**Udhëzime se si të largoni persona nga dosjet e përbashkëta në shërbime të ndryshme në internet:**

- [Facebook](https://www.facebook.com/help/211909018842184/)
- - [Google Drive](https://support.google.com/drive/answer/2494893?hl=en&co=GENIE.Platform=Desktop)
- [Dropbox](https://www.dropbox.com/help/files-folders/unshare-folder#remove-member)
- [iCloud](https://support.apple.com/en-us/HT201081)


### Fshirja e informacioneve nga profilet

Në disa raste mund të dëshironi ta hiqni përmbajtjen nga grafiku kohor në mediat sociale të të arrestuarit ose nga burimet e tjera që lidhen me llogarinë e tyre, sepse mund të abuzohet si provë kundër tyre ose të krijojë konfuzion dhe konflikt brenda bashkësisë së të arrestuarit ose t’i diskreditojë ata.

Disa shërbime e bëjnë më të lehtë fshirjen e burimeve dhe postimeve nga llogaritë dhe grafiket kohore. Lidhjet për udhëzimet për Twitter, Facebook dhe Instagram janë dhënë më poshtë. Ju lutemi sigurohuni që keni dokumentuar përmbajtjen që dëshironi të fshini në rast se mund të keni nevojë përsëri për të si dëshmi në rast të ndërhyrjes etj..

- Për Twitter, mund të përdorni [Tweet Deleter](https://tweetdeleter.com/).
- Për Facebook, mund të ndiqni [këtë udhëzues](https://medium.com/@louisbarclay/how-to-delete-your-facebook-news-feed-6c99e51f1ef6), bazuar në një aplikacion për shfletuesin Chrome të quajtur [Nudge](https://chrome.google.com/webstore/detail/nudge/dmhgdnbkjkejeddddlklojinngaideac).
- Për Instagram, mund të ndiqni [këto udhëzime](https://help.instagram.com/997924900322403) (por kini kujdes që njerëzit me qasje në përzgjedhjet e llogarisë tuaj do të jenë në gjendje të shohin historinë e shumicës së ndërveprimeve tuaja edhe pas fshirjes së përmbajtjes).


### Fshini shoqërimet e dëmshme në internet

Nëse ka ndonjë informacion në internet ku përmendet emri i të arrestuarit që mund të ketë pasoja negative për të ose kontaktet e saj, është ide e mirë ta fshini nëse kjo nuk do të dëmtojë më tej të arrestuarin.

- Bëni një listë të hapësirave dhe informacioneve në internet që duhet të hiqen ose të ndryshohen.
- Nëse keni identifikuar përmbajtjen që duhet të hiqet ose të ndryshohet, mund të dëshironi që të bëni kopje rezervë përpara se të vazhdoni me fshirjen e saj ose me kërkesat për heqjen e saj.
- Vlerësoni nëse heqja e emrit të të arrestuarit mund të ketë ndikim negativ në situatën e tyre (p.sh. heqja e emrave të tyre nga lista e punonjësve të një organizate mund ta mbrojë organizatën, por gjithashtu mund ta heqë arsyetimin për të arrestuarin, për shembull se ata kanë punuar për atë organizatë).
- Nëse keni qasje në ueb-faqet ose llogaritë përkatëse, ndryshoni ose hiqni përmbajtjen dhe informacionin e ndjeshëm.
 - Nëse nuk keni qasje, kërkojuni njerëzve që kanë qasje të heqin informacionin e ndjeshëm.
- Udhëzime për heqjen e përmbajtjes në shërbimet e Google mund të gjeni [këtu](https://support.google.com/webmasters/answer/6332384?hl=en#get_info_off_web)
- Kontrolloni nëse faqet e internetit që përmbajnë informacion janë indeksuar nga [Wayback Machine](https://web.archive.org) ose [Google Cache](https://cachedview.com/). Nëse po, edhe kjo përmbajtje duhet të hiqet.
	- [Si të kërkoni heqjen nga Machine Wayback] https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
	- [Hiqni një faqe të vendosur në sajtin tuaj nga Google] https://developers.google.com/search/docs/crawling-indexing/remove-information)

<a name="online_bank_accounts"></a>
### Llogaritë bankare në internet

Shumë shpesh, llogaritë bankare menaxhohen dhe mund të qasen në internet, dhe verifikimi përmes pajisjeve mobile mund të jetë i nevojshëm për transaksione ose madje edhe mënyra e vetme për të hyrë në llogarinë online. Nëse i arrestuari nuk mund të hyjë në llogaritë e saj bankare për një periudhë më të gjatë, kjo mund të ketë pasoja për gjendjen financiare të të arrestuarit dhe aftësinë e tyre për të hyrë në llogari. Në këto raste, sigurohuni që keni:

- Shkëputur pajisjet e sekuestruara nga llogaria/të bankare e të arrestuarit.
- Merrni autorizim dhe prokurë nga i arrestuari për të qenë në gjendje të administroni llogarinë bankare në emrin e tyre që në fillim të procesit (në marrëveshje me të afërmit e tyre).


## Këshillat përfundimtare

- Sigurohuni që t'ia ktheni të gjithë pronësinë e të dhënave të të arrestuarit pas lirimit të tyre.
- Lexoni [këto këshilla në sekuencën e veprimeve "Kam humbur pajisjen time"](../topics/lost-device/questions/device-returned) se si t'i trajtoni pajisjet e sekuestruara pasi ato të kthehen nga autoritetet.
