---
layout: page
title: PGP
author: mfc
language: en
summary: Metodat e kontaktit
date: 2020-11
permalink: /en/contact-methods/pgp.md
parent: /en/
published: true
---

PGP (ose Pretty Good Privacy) dhe ekuivalenti i tij me burim të hapur, GPG (Gnu Privacy Guard), ju lejon të kriptoni përmbajtjen e e-maileve për të mbrojtur mesazhin që të mos mund të shihet nga ofruesi juaj i e-mailit ose çdo palë tjetër që mund të ketë qasje në atë e-mail. Megjithatë, fakti që ju i keni dërguar një mesazh organizatës marrëse mund të jetë i aksesueshëm nga qeveritë ose agjencitë e zbatimit të ligjit. Për ta parandaluar këtë, mund të krijoni një adresë e-maili alternative që nuk lidhet me identitetin tuaj.

Burimet: [Access Now Helpline Community Documentation: Secure Email: E-maili i sigurt](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Fondacioni i Lirisë së Shtypit: Kriptimi i emailit me Mailvelope: Udhëzues për fillestarët](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Mjetet e privatësisë: Ofruesit e e-mailit privat](https://www.privacytools.io/providers/email/)
