---
layout: page
title: "Estou sofrendo doxxing ou alguém está compartilhando minhas mídias ou informações privadas sem a minha permissão "
author: Ahmad Gharbeia, Michael Carbone, Gus Andrews, Flo Pagano, Constanza Figueroa, Alex Argüelles
language: en
summary: "Estão circulando informações sobre mim ou imagens minhas sem a minha permissão. Isso é perturbador para mim ou pode me prejudicar"
date: 2023-05
permalink: /pt/topics/doxing
parent: Home
---

# Alguém está compartilhando minhas mídias ou informações privadas sem a minha permissão.


Em alguns casos, assediadores podem publicar informações que te identifiquem ou que exponham aspectos da sua vida pessoal que você tenha escolhido manter privados. Essa prática é chamada de "doxxing" ou "doxing" (uma abreviação para "divulgação de documentos" em inglês).

O objetivo do doxxing é constranger o alvo ou conspirar contra ele. Isso por ter efeitos negativos no seu bem-estar psicossocial, segurança pessoal, relacionamentos e trabalho.

Doxxing é utilizado para intimidar figuras públicas, jornalistas, defensores de direitos e ativistas, mas também visa comumente minorizadas e populações marginalizadas. Está diretamente relacionado a outras formas de violência online e de gênero.

Quem pratica o doxxing pode ter obtido suas informações de várias fontes, incluindo invasões em suas contas e dispositivos privados. Também é possível que estejam expondo e tirando de contexto informações ou mídias que você compartilhou com amigues, parceires, conhecides ou colegas de trabalho, através de meios que você considerava privados/seguros. Em alguns casos assediadores podem agregar informações disponíveis publicamente sobre você nas redes sociais ou em registros públicos. A combinação dessas informações em novas construções, mal interpretadas, também pode constituir doxxing.

Se você foi ou está sendo vítima de doxxing, recomendamos que utilize o seguinte questionário para identificar onde suas informações foram encontradas e buscar formas de mitigar os danos, incluindo a remoção dos documentos de sites na internet:


## Workflow

### be_supported

> A documentação desempenha um papel fundamental na avaliação dos impactos, na identificação da origem das agressões e no desenvolvimento de respostas que restabeleçam sua segurança. Dependendo do ocorrido, o processo de documentação pode intensificar o impacto emocional dessa agressão em você. Para aliviar a carga desse trabalho e reduzir o estresse emocional que isso causa, você pode necessitar do apoio de amigues próximos ou de outra pessoa em quem confie.

Sente que tem alguém em quem confia que pode te ajudar se precisar?

 - [Sim, uma pessoa de confiança está pronta para me ajudar. ](#physical_risk)

### physical_risk

> Reflita: quem você acha que pode estar por trás desses ataques? Do que essa pessoa é capaz? Até que ponto está determinada a te fazer mal? Quem mais pode estar envolvide? Essas questões vão te ajudar a avaliar as ameaças e a determinar se está exposto a riscos físicos.

Você teme pela sua integridade física ou bem-estar, ou pensa que pode estar sob ameaça legal?

 - [Sim, penso que estou exposte a riscos físicos.](#yes_physical_risk)
 - [Penso que posso estar em risco legal.](#legal_risk)
 - [Não, gostaria de resolver o meu problema na esfera digital.](#takedown_content)
 - [Não tenho certeza. Preciso de ajuda para avaliar os riscos.](#assessment_end)

### yes_physical_risk

> Se sentir que pode ser alvo de ataques pessoais, ou que a sua integridade física, ou bem-estar estão em risco, pode consultar os seguintes recursos para refletir sobre as suas necessidades imediatas de segurança física:
>
> - [Front Line Defenders - Manual de Segurança](https://www.frontlinedefenders.org/pt/manual-de-seguran%C3%A7a)
> - [National Domestic Violence Hotline - Crie um Plano Seguro](https://www.thehotline.org/plan-for-safety/create-a-safety-plan/) (em inglês)
> - [Coalition against Online Violence - Apoio para Segurança Física](https://onlineviolenceresponsehub.org/physical-security-support) (em inglês)
> - [Cheshire Resilience Forum - Como se preparar para uma emergência](https://cheshireresilience.org.uk/how-to-prepare-for-an-emergency/) (em inglês)
> - [FEMA Form P-1094 Crie o Seu Plano Familiar de Comunicação em Emergências](https://www.templateroller.com/group/12262/create-your-family-emergency-communication-plan.html) (em inglês)
> - [Reolink - Como Proteger de Maneira Inteligente as Janelas da sua Casa — Top 9 soluções fáceis e seguras](https://reolink.com/blog/top-7-easy-diy-ways-to-secure-your-home-windows/) (em inglês)
> - [Reolink - Como Proteger sua Casa de Arrombamentos](https://reolink.com/blog/make-home-safe-from-break-ins/) (em inglês)
> - [Segurança Integral pra Jornalistas - Segurança Física](https://seguridadintegral.articulo19.org/categorias-prevencion/seguridad-fisica/) (em espanhol)

Precisa de mais ajuda para garantir sua integridade física e o seu bem-estar?

 - [Sim](#physical-risk_end)
 - [Não, mas acho que também estou sob risco legal](#legal_risk)
 - [Não, gostaria de resolver meu problema na esfera digital](#takedown_content)
 - [Não tenho certeza. Preciso de ajuda para avaliar os riscos.](#assessment_end)

### legal_risk

> Se estiver pensando em entrar com uma ação judicial, será muito importante guardar provas dos ataques vieram do alvo. Por isso, sugerimos que siga as [recomendações da página do Kit de Primeiros Socorros Digital sobre o registo de informações sobre ataques.](/../../documentation).

Precisa de apoio imediato?

 - [Sim](#legal_end)
 - [Não, gostaria de resolver meu problema na esfera digital](#takedown_content)
 - [Não](#resolved_end)

### takedown_content

Quer trabalhar para que os documentos compartilhados indevidamente sejam retirados do ar?

 - [Sim](#documenting)
 - [Não](#resolve_end)

### documenting

> É muito importante manter sua própria documentação sobre o ataque em que você foi vítima. Isso facilita a assistência jurídica ou técnica. Consulte o guia do [Kit de Primeiros Socorros Digitais sobre documentação de ataques online](/../../documentation) para saber qual a melhor forma de reunir informações para suas necessidades específicas.
>
> Você pode também pedir para que uma pessoa de confiança te ajude a procurar evidências do ataque. Isso te ajuda a não revisitar traumas ao navegar pelo conteúdo mais de uma vez.

Se sente preparade para documentar o ataque de forma segura?

- [Sim, estou documentando os ataques.](#where_published)

### where_published

> A resposta adequada a um ataque dependerá do local onde suas informações foram compartilhadas.
>
> Se suas informações foram publicadas em plataformas de redes sociais sediadas em países com sistemas jurídicos que estabelecem responsabilidades legais para as empresas em relação aos usuários de seus serviços, como a União Europeia ou os Estados Unidos, é provável que os moderadores da empresa estejam dispostos a te ajudar. Para isso, você precisará apresentar provas documentais dos ataques. É recomendado pesquisar o país em que a empresa está sediada para obter informações mais específicas.
>
> No entanto, em alguns casos, agressores podem publicar suas informações em sites menores e menos conhecidos, que muitas vezes estão hospedados em países onde não há proteção legal adequada ou são administrados por pessoas ou grupos que apoiam ou praticam comportamentos nocivos. Nesses casos, pode ser mais difícil acionar os sites onde o material está sendo publicado ou identificar seus administradores.
>
> Mesmo que os responsáveis pelo ataque tenha publicado seus documentos em outro local, além das redes sociais populares (o que acontece muito), trabalhar em conjunto com esses espaços para retirar esses materiais ou os links pode reduzir muito a quantidade de pessoas vendo suas informações. Reduz inclusive a quantidade e a gravidade dos ataques que você pode ser vítima.

Minhas informações foram publicadas em

- [Uma plataforma ou serviço regulamentado por lei ](#what_info_published)
- [Uma plataforma ou serviço que provavelmente não irá responder pedidos de derrubada](#legal_advocacy)

### what_info_published

Quais informações foram compartilhadas sem a sua permissão?

- [Informações pessoais que podem me identificar ou informações privadas como: endereço, número de telefone, CPF, conta bancária, etc.](#personal_info)
- [Mídias que eu não permiti o compartilhamento, incluindo conteúdo íntimo (fotos, vídeos, áudios, texto, etc.)](#media)
- [Um pseudônimo que utilizo foi relacionado com a minha identidade real ](#linked_identities)

### linked_identities

> Se um agressor vincular publicamente seu nome ou identidade real a um pseudônimo, apelido ou nome de usuárie que você usa para se expressar onlineou se organizar em ativismo, é importante considere a possibilidade de encerrar as contas associadas a essa identidade alternativa como uma medida de reduzir danos.

Você precisa continuar utilizando essa identidade/personagem ou pode encerrar a(s) conta(s)?

- [Preciso continuar utilizando ](#defamation_flow_end)
- [Posso encerrar a(s) conta(s)](#close)

### close

> Antes de encerrar as contas associadas à identidade ou persona afetada, considere os riscos. Você poderá perder o acesso a serviços ou dados, enfrentar riscos sobre a sua reputação, perder o contato com associades online, etc.

- [Encerrei minhas contas relevantes](#resolved_end)
- [Decidi não encerrar as contas nesse momento](#resolved_end)
- [Preciso de ajuda para decidir o que fazer](#harassment_end)

### personal_info

Onde suas informações pessoais foram publicadas?

- [Em uma plataforma de rede social](#doxing-sn)
- [Em um website](#doxing-web)

### doxing-sn

> Se suas informações privadas tiverem sido publicadas em uma plataforma de redes sociais, você pode reportar uma violação das normas da comunidade, seguindo os procedimentos de denúncia fornecidos nos sites das plataformas.
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
>Na lista seguinte, você encontra instruções para reportar nas principais plataformas violações das normas da comunidade:
>
> - [Google](https://support.google.com/websearch/answer/9172218?hl=pt-BR)
> - [Facebook](https://pt-br.facebook.com/help/1380418588640631)
> - [Twitter](https://help.twitter.com/pt/rules-and-policies/enforcement-options)
> - [Tumblr](https://www.tumblr.com/policy/br/community)
> - [Instagram](https://pt-br.facebook.com/help/instagram/192435014247952)
>
> Lembre-se que pode demorar algum tempo para receber uma resposta para as solicitações. Salve essa página nos favoritos do seu navegador e retorne sempre que precisar.

As informações foram deletadas?

 - [Sim](#one-more-persons)
 - [Não](#harassment_end)

### doxing-web

> Você pode tentar denunciar o site ao seu fornecedor de hospedagem e domínio, solicitando a remoção.
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
> Para enviar um pedido de retirada de conteúdo, você vai precisar coletar informações sobre o site em que suas informações foram publicada:
>
> - Acesse o serviço [Network Tools NSLookup](https://network-tools.com/nslookup/) (em inglês) para encontrar o(s) endereço(s) IP de sites falsos. Basta inserir a URL do site no campo de pesquisa.
> - Anote o(s) endereço(s) IP.
> - Acesse o serviço [Doamin Tools' Whois Lookup](https://whois.domaintools.com/) (em inglês) e pesquise tanto pelo IP quanto pelo domínio do site falso.
> - Guarde o nome e o endereço de e-mail do provedor de hospedagem e domínio. Se estiver incluído na pesquisa, registre também o nome do proprietário do site.
> - Escreva para o fornecedor de hospedagem e domínio do site, para solicitar a remoção. Inclua na mensagem o endereço IP, o URL e o proprietário do site, bem como as razões pelas quais ele é abusivo.
>
> Lembre-se que pode demorar algum tempo para receber uma resposta para as solicitações. Salve essa página nos favoritos do seu navegador e retorne sempre que precisar.

O conteúdo foi removido?

- [Sim](#resolved_end)
- [Não, a plataforma não respondeu/ajudou](#platform_help_end)
- [Preciso de ajuda para saber como enviar um pedido de remoção](#platform_help_end)

### media

> Se foi você que criou a mídia, normalmente você detém seus os direitos autorais. Em algumas jurisdições, pessoas também têm direito sobre as mídias em que aparece (direitos de imagem)

Você detém os direitos autorais da mídia?

- [Sim](#intellectual_property)
- [Não](#nude)

### intellectual_property

> Você pode acionar mecanismos de direitos autorais para derrubar conteúdos difamatórios. Busque compreender as políticas de direitos autorais do seu país ou busque o apoio de especialistas. A maioria das redes sociais oferece formulários para denúncias de violação de direitos autorais e através deles a reposta pode ser mais rápida, por conta das implicações legais que podem sofrer.
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
> Você pode também utilizar esses links para enviar um pedido de retirada de conteúdo por violação de direitos autorais para as principais plataformas de redes sociais:
>
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/pt/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/) (em inglês)
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos favoritos do seu navegador para retornar sempre que precisar.

O conteúdo foi removido?

- [Sim](#resolved_end)
- [Não, a plataforma não me respondeu/ajudou](#platform_help_end)
- [Não encontrei nas opções acima a plataforma que preciso](#platform_help_end)

### nude

> Mídias que mostram nudez muitas vezes são cercadas pelas políticas das próprias plataformas.

A mídia inclui corpos nus ou detalhes?

- [Sim](#intimate_media)
- [Não](#nonconsensual_media)

### intimate_media

> Para aprender a reportar uma violação das regras de privacidade ou compartilhamento de mídias íntimas/conteúdo degradante, sem sua permissão, nas plataformas mais populares, veja as seguintes ferramentas:
>
> - [Safernet - Helpline](https://www.helpline.org.br)
> - [Stop NCII](https://stopncii.org/) (Facebook, Instagram, TikTok and Bumble - em todo o mundo)
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos favoritos do seu navegador para retornar sempre que precisar.

O conteúdo foi removido?

- [Sim](#resolved_end)
- [Não, a plataforma não me respondeu/ajudou](#platform_help_end)
- [Não encontrei nas opções acima a plataforma que preciso](#platform_help_end)

### nonconsensual_media

Onde as mídias foram publicadas?

- [Plataformas de redes sociais](#NCII-sn)
- [Em um site](#NCII-web)

### NCII-sn

> Se a sua mídia tiver sido publicada em uma plataforma de rede social, denuncie uma violação das normas da comunidade, seguindo os procedimentos de denúncia fornecidos por elas mesmo.
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
> Na lista a seguir, você vai encontrar instruções para esse tipo de denúncia em cada uma das plataformas:
>
> - [Google](https://support.google.com/websearch/answer/9172218?hl=pt-BR)
> - [Facebook](https://pt-br.facebook.com/help/1380418588640631)
> - [Twitter](https://help.twitter.com/pt/rules-and-policies/enforcement-options)
> - [Tumblr](https://www.tumblr.com/policy/br/community)
> - [Instagram](https://pt-br.facebook.com/help/instagram/192435014247952)
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos favoritos do seu navegador para retornar sempre que precisar.

O conteúdo foi removido?

- [Sim](#resolved_end)
- [Não, a plataforma não me respondeu/ajudou](#platform_help_end)
- [Não encontrei nas opções acima a plataforma que preciso ](#platform_help_end)

### NCII-web

> Siga as instruções em ["Without My Consent - Take Down"](https://withoutmyconsent.org/resources/take-down) (em inglês) para tirar conteúdo de um site.
>
> ***Nota:*** *Sempre [documente](/../../documentation) antes de tomar medidas como excluir mensagens ou registros de conversas e bloquear perfis. Se estiver considerando uma ação judicial, consulte informações sobre [documentação legal](/../../documentation#legal).*
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos favoritos do seu navegador para retornar sempre que precisar.

O conteúdo foi removido?

- [Sim](#resolved_end)
- [Não, a plataforma não me respondeu/ajudou](#platform_help_end)
- [Não encontrei nas opções acima a plataforma que preciso](#platform_help_end)

### legal_advocacy

> Em alguns casos, as plataformas onde ocorrem doxxing ou publicações de conteúdo íntimo sem consentimento podem não ter políticas ou medidas adequadas para lidar com pedidos de remoção de conteúdo relacionados a assédio ou difamação.
>
> Por vezes, o material ofensivo está publicado ou hospedado em plataformas ou aplicações geridas por indivídes ou grupos que praticam, apoiam ou simplesmente não se opõem a comportamentos nocivos.
>
> Também existem locais no ciberespaço onde as leis e as regulações normais dificilmente são cumpridas, porque desenhados para funcionar anonimamente e não deixar rastros. Um desses espaços é conhecido por *"dark web"*. Existem benefícios no anonimato e na existência de espaços como esse, mas mesmo assim algumas pessoas podem abusar deles.
>
> Em situações como essa, em que implicações legais são impossíveis, reflita sobre a sua disposição em recorrer às campanhas de sensibilização. Isto pode incluir o esclarecimento público do seu caso, a criação de um debate público sobre o mesmo e, eventualmente, a divulgação de casos semelhantes. Essas são organizações e grupos voltados para a defesa de direitos, que podem te ajudar nesse processo. Podem te ajudar a entender o que você pode esperar e esclarecer possíveis desdobramentos.
>
> Procurar apoio jurídico pode ser o último recurso se a comunicação com as plataformas, apps ou serviços não resolver o problema.
>
> O caminho por vias jurídicas normalmente demanda tempo, custa dinheiro e muitas vezes a necessidade de publicizar que você foi vítima de doxxing, que suas mídias íntimas foram expostas sem o seu consentimento. O sucesso de um processo na justiça depende de diversos fatores como uma boa documentação do ataque, de forma que seja aceita nos tribunais. Identificar agressores e determinar que são de fato responsáveis pode difícil de provar. Além disso, estabelecer qual a jurisdição em que o caso deve ser julgado pode também ser um desafio.

O que você quer fazer?

- [Preciso de apoio para planejar uma campanha de sensibilização](#advocacy_end)
- [Preciso de apoio jurídico](legal_end)
- [Tenho apoio da minha comunidade local](#resolved_end)

### advocacy_end

> Se quiser contestar informações que foram espalhadas online sobre você sem seu consentimento, sugerimos que consulte as recomendações na seção [Campanhas de Difamação, aqui do Kit de Primeiros Socorros Digitais.](../../../defamation).
>
> Se quiser apoio para lançar uma campanha para expor agressores, entre em contato com organizações que podem ajudar nessas representações.

:[](organisations?services=advocacy)


### legal_end

> Se precisa de apoio legal, entre em contato com as organizações abaixo, elas podem te ajudar.
>
> Se está pensando em tomar providências legais, é muito importante que levante evidências dos ataques dos quais foi vítima. Para isso, incentivamos que consulte as [recomendações sobre documentação de ataques, aqui do Kit de Primeiros Socorros Digitais.](/../../documentation).

:[](organisations?services=legal)

### physical-risk_end

> Se estiver em risco físico e precisar de ajuda imediata, acione as organizações abaixo indicadas que podem te apoiar.

:[](organisations?services=physical_security)


### assessment_end

> Se achar que precisa de ajuda para avaliar os riscos que pode estar correndo por conta da exposição de suas informações e mídias, entre em contato com as organizações abaixo.

:[](organisations?services=harassment&services=assessment)

### resolved_end

> Esperamos que este guia de resolução de problemas tenha sido útil. Por favor, nos envie seus comentários [por e-mail].(mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### defamation_flow_end

> Se quiser contestar informações que foram espalhadas online sobre você sem seu consentimento, sugerimos que consulte as recomendações na seção [Campanhas de Difamação, aqui do Kit de Primeiros Socorros Digitais](../../../defamation).
>
> Se precisa de apoio especializado, a lista abaixo inclui organizações que podem te ajudar em questões envolvendo reputação.

:[](organisations?services=advocacy&services=legal)

### platform_help_end

> Abaixo está uma lista de organizações que podem te apoiar no processo de denúncia às plataformas e serviços.

:[](organisations?services=harassment&services=legal)

### harassment_end

> Precisando de apoio para resolver sua situação, entre em contato com as seguintes organizações que podem te ajudar.

:[](organisations?services=harassment&services=triage)


### final_tips

- Mapeie sua presença online. O self-doxing consiste em explorar informações sobre você em locais públicos no ambiente digital, para evitar que agentes maliciosos descubram e utilizem essas informações para se fazerem passar por você. Aprenda como pesquisar seus rastros online no [Guia de Prevenção conta Doxxing, da Acess Now Helpline's](https://guides.accessnow.org/self-doxing.html) (em inglês), também neste [artigo da CPJ](https://cpj.org/pt/2020/11/seguranca-digital-guias-para-proteger-se/).
- Se o doxxing envolveu informações ou documentos que só você ou poucas pessoas de confiança conhecem ou teriam acesso, reflita sobre como agressores podem ter descoberto. Reveja suas práticas de segurança de informações pessoais, para melhorar a proteção de seus dispositivos e contas.
- Em um futuro próximo, atenção para a possibilidade do conteúdo ser vazado novamente na internet. Uma forma de se proteger é procurar seu nome ou pseudônimos nos mecanismos de busca para saber onde estão aparecendo. Crie um alerta do Google para você, ou utilize o Meltwater, ou o Mention. Esses serviços também vão te notificar caso você apareça na internet.

#### resources

- [O que é, qual é a legalidade e como prevenir doxxing?](https://www.avast.com/pt-br/c-what-is-doxxing)
- [Crash Override Network - So You’ve Been Doxed: A Guide to Best Practices](https://crashoverridenetwork.tumblr.com/post/114270394687/so-youve-been-doxed-a-guide-to-best-practices) (em inglês)
- [Access Now Helpline Community Documentation: Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html) (em inglês)
- [Equality Labs: Anti-doxing Guide for Activists Facing Attacks from the Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c) (em inglês)
- [FemTechNet: Locking Down Your Digital Identity](https://femtechnet.org/csov/lock-down-your-digital-identity/) (em inglês)
- [Ken Gagne, Doxxing defense: Remove your personal info from data brokers](https://www.computerworld.com/article/2849263/doxxing-defense-remove-your-personal-info-from-data-brokers.html) (em inglês)
- [Totem Project - Keep it private](https://learn.totem-project.org/courses/course-v1:IWMF+IWMF_KP_EN+001/about) (em inglês)
- [Totem Project - How to protect your identity online](https://learn.totem-project.org/courses/course-v1:Totem+TP_IO_EN+001/about) (em inglês)
- [Coalition against Online Violence - I've been doxxed](https://onlineviolenceresponsehub.org/for-journalists#doxxed) (em inglês)
- [PEN America: Online Harassment Field Manual](https://onlineharassmentfieldmanual.pen.org/) (em inglês e espanhol)
- [Guia Menina em Rede](https://www.safernet.org.br/guiameninaemrede.pdf)
