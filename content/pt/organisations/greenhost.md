---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 horas
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

A Greenhost oferece serviços de tecnologia através de uma abordagem ética e sustentável. Seus serviços incluem hospedagem web, serviços de nuvem e ofertas potentes para o nicho de segurança da informação. A Greenhost está ativamente envolvida com o desenvolvimento de código aberto e faz parte de vários projetos nas áreas de tecnologia, jornalismo, cultura, educação, sutentabilidade e liberdade da internet.
