---
layout: page
title: "Saya Kehilangan Data Saya"
author: Abir Ghattas, Alexandra Hache, Ramy Raoof
language: id
summary: "Apa yang harus dilakukan jika Anda kehilangan sejumlah data"
date: 2023-04
permalink: /id/topics/lost-data
parent: Home
---

# Saya Kehilangan Data Saya

Data digital bisa bersifat sangat sementara dan tidak stabil, serta ada banyak cara Anda bisa kehilangannya. Kerusakan fisik peranti, penghentian akun, kekeliruan penghapusan, pembaruan perangkat lunak dan kerusakan perangkat lunak dapat menyebabkan hilangnya data Anda. Di samping itu, kadang Anda mungkin tidak mengetahui bagaimana sistem pencadangan (*backup*) bekerja dan apakah sistemnya bekerja seperti seharusnya, atau Anda hanya lupa kredensial atau rute untuk mencari atau memulihkan data Anda.

Bagian Pertolongan Pertama Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mendiagnosis bagaimana Anda bisa kehilangan data serta kemungkinan strategi penanggulangan untuk memulihkannya.

*Pada bagian ini kami akan berfokus pada data berbasis-peranti. Untuk konten dan kredensial daring, Anda dapat mengikuti [alur kerja Pertolongan Pertama Darurat Digital pada masalah akses akun](../account-access-issues).*

Berikut adalah kuesioner untuk mengidentifikasi masalah Anda dan menemukan solusi yang memungkinkan.

## Workflow

### entry-point

> Di bagian ini kami akan lebih berfokus pada data berbasis-peranti. Untuk konten dan kredensial daring, kami akan mengarahkan Anda ke bagian lain dari Pertolongan Pertama Darurat Digital.
>
> Peranti yang dimaksud di sini meliputi komputer, peranti seluler, diska keras eksternal, stik USB, dan kartu SD.

Anda kehilangan jenis data apa?

- [Konten daring](../../../account-access-issues)
- [Kredensial](../../../account-access-issues)
- [Konten di peranti](#content-on-device)

### content-on-device

> Seringkali data Anda yang "hilang" adalah data yang telah terhapus - baik disengaja ataupun tidak. Periksa *Recycle Bin* atau Keranjang Sampah di komputer Anda. Di peranti Android, Anda mungkin bisa menemukan data yang hilang dalam direktori LOST.DIR. Jika berkas yang hilang tidak bisa ditemukan di *Recycle Bin* atau Keranjang Sampah sistem operasi Anda, mungkin data tersebut belum terhapus, tapi ada di lokasi yang berbeda dari yang Anda kira. Cobalah fungsi pencarian di sistem operasi Anda untuk menemukannya.
>
> Coba cari nama persis dari berkas Anda yang hilang. Jika tidak berhasil, atau jika Anda tidak yakin dengan nama persisnya, Anda dapat mencoba pencarian *wildcard* (yaitu jika Anda kehilangan berkas docx, tetapi tidak yakin dengan namanya, Anda dapat mencari dengan memberi tanda * atau ? di depan ekstensi berkas, misalnya `*.docx`) yang hanya akan menampilkan hasil pencarian dengan ekstensi `.docx`. Urutkan berdasarkan *Tanggal Dimodifikasi* untuk menemukan berkas terbaru dengan cepat saat menggunakan opsi pencarian *wildcard*.
>
> Selain mencari data yang hilang di peranti Anda, tanyakan pada diri sendiri apakah Anda memiliki cadangan data itu, atau Anda telah mengirimnya melalui surel atau membagikannya dengan seseorang (termasuk Anda sendiri), atau menambahkannya ke penyimpanan *cloud* Anda kapan saja. Jika demikian, Anda dapat melakukan pencarian di sana dan mungkin mendapatkan berkasnya kembali atau beberapa versinya.
>
> Untuk meningkatkan peluang Anda memulihkan data, segera hentikan penyuntingan dan penambahan informasi ke peranti Anda. Jika bisa, sambungkan *drive* sebagai peranti eksternal (misalnya melalui USB) ke komputer terpisah yang aman untuk mencari dan memulihkan informasi. Menulis data ke *drive* (misalnya mengunduh atau membuat dokumen baru) dapat mengurangi kemungkinan menemukan dan memulihkan data yang hilang.
>
> Periksa juga berkas dan folder tersembunyi, karena data yang hilang mungkin ada di sana. Berikut cara Anda dapat melakukannya di [Mac (dalam bahasa Inggris)](https://www.macworld.co.uk/how-to/mac-software/show-hidden-files-mac-3520878/) dan [Windows](https://support.microsoft.com/id-id/windows/view-hidden-files-and-folders-in-windows-97fbc472-c603-9d90-91d0-1166d1d9f4b5). Untuk Linux, buka direktori *home* Anda di pengelola berkas dan tekan Ctrl + H.

Bagaimana Anda kehilangan data Anda?

- [Peranti penyimpanan data mengalami kerusakan fisik](#tech-assistance_end)
- [Peranti penyimpanan data data dicuri/hilang](#device-lost-theft_end)
- [Datanya terhapus](#where-is-data)
- [Datanya menghilang setelah pembaruan perangkat lunak](#software-update)
- [Sistem atau alat perangkat lunak rusak dan datanya hilang](#where-is-data)


### software-update

> Terkadang, saat Anda memperbarui atau memutakhirkan perangkat lunak atau keseluruhan sistem operasi, masalah tak terduga dapat terjadi, menyebabkan sistem atau perangkat lunak berhenti bekerja sebagaimana mestinya, atau berbagai perilaku yang tidak seharusnya terjadi, termasuk kerusakan data. Jika Anda kehilangan data tepat setelah pembaruan perangkat lunak atau sistem, ada baiknya mempertimbangkan untuk melakukan *roll back* untuk kembali ke keadaan sebelumnya. *Roll back* bisa berguna karena ini berarti perangkat lunak atau basis data Anda dapat dipulihkan ke keadaan yang bersih dan konsisten bahkan setelah terjadi kekeliruan operasi atau kerusakan perangkat lunak.
>
> Metode untuk melakukan *roll back* pada perangkat lunak ke versi sebelumnya tergantung pada bagaimana perangkat lunak tersebut dibangun - pada beberapa kasus hal ini bisa dilakukan dengan mudah, di kasus lain tidak, dan pada beberapa kasus dibutuhkan usaha yang lebih. Jadi Anda perlu melakukan pencarian daring mengenai *roll back* ke versi yang lebih lama dari perangkat lunak tersebut. Harap perhatikan bahwa melakukan *roll back* juga dikenal sebagai "menurunkan versi", jadi lakukan pencarian dengan istilah ini juga. Untuk sistem operasi seperti Windows atau Mac, masing-masing memiliki metodenya sendiri untuk menurunkan ke versi sebelumnya.
>
> - Untuk sistem Mac, kunjungi [Basis pengetahuan Pusat Dukungan Mac](https://support.apple.com/id-id) dan gunakan kata kunci "pemulihan" dengan versi macOS Anda untuk membaca lebih lanjut dan menemukan instruksinya.
> - Untuk sistem Windows, prosedurnya bervariasi tergantung pada apakah Anda ingin membatalkan pembaruan tertentu atau melakukan *roll back* pada seluruh sistem. Di kedua kasus tersebut, Anda dapat menemukan petunjuk di [Pusat Dukungan Microsoft](https://support.microsoft.com/id-ID), misalnya untuk Windows 10 Anda dapat mencari pertanyaan "Bagaimana cara menghapus pembaruan yang sudah diinstal" di [laman Pertanyaan Umum](https://support.microsoft.com/id-ID/help/12373/windows-update-faq).

Apakah cara "roll back" perangkat lunaknya bermanfaat?

- [Ya](#resolved_end)
- [Tidak](#where-is-data)


### where-is-data

> Pencadangan data (*backup*) secara berkala adalah kebiasaan yang baik dan direkomendasikan. Kadang kita lupa jika kita telah mengaktifkan pencadangan otomatis atau belum, jadi sebaiknya periksalah peranti Anda untuk melihat apakah opsi tersebut telah diaktifkan, dan gunakan cadangan data tersebut untuk memulihkan data Anda. Jika Anda belum mengaktifkannya, disarankan untuk merencanakan pencadangan data di masa yang mendatang, dan Anda dapat menemukan lebih banyak tip tentang cara mengatur cadangan data di [tip terakhir dari alur kerja ini](#resolved_end).

Untuk memeriksa apakah Anda memiliki cadangan dari data yang hilang, mulai tanyakan pada diri sendiri di mana data yang hilang tersebut disimpan.

- [Di peranti penyimpanan (diska keras eksternal, stik USB, kartu SD)](#storage-devices_end)
- [Di komputer](#computer)
- [Di peranti seluler](#mobile)

### computer

Jenis sistem operasi apa yang Anda gunakan di komputer Anda?

- [MacOS](#macos-computer)
- [Windows](#windows-computer)
- [Gnu/Linux](#linux-computer)

### macos-computer

> Untuk memeriksa apakah peranti MacOS Anda mengaktifkan opsi pencadangan, dan menggunakan cadangan data tersebut untuk memulihkan data Anda, periksa opsi [iCloud](https://support.apple.com/id-id/HT208682) atau [Time Machine](https://support.apple.com/id-id/HT201250) Anda untuk melihat apakah ada cadangan data yang tersedia.
>
> Salah satu tempat yang perlu diperiksa adalah daftar *Recent Items* yang terus berjalan, yang mana hal ini melacak aplikasi, berkas dan *server* yang telah Anda gunakan selama beberapa sesi terakhir di komputer. Untuk mencari berkas tersebut dan membukanya kembali, buka Menu Apple di pojok kiri atas, pilih *Recent Items* dan telusuri daftar berkas. Detail lebih lanjut dapat ditemukan di [Panduan resmi pengguna macOS](https://support.apple.com/id-id/guide/mac-help/mchlp2305/mac).

Apakah Anda dapat menemukan data Anda atau memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#macos-restore)

### macos-restore

> Di beberapa kasus ada beberapa perangkat gratis dan sumber-terbuka yang dapat membantu menemukan konten yang hilang dan memulihkannya. Terkadang penggunaannya terbatas, dan sebagian besar perangkat yang terkenal berbayar.
>
> Sebagai contoh, Anda bisa mencoba [EaseUS Data Recovery Wizard](https://www.easeus.com/mac-data-recovery-software/drw-mac-free.htm) atau [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/).

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### windows-computer

> Untuk memeriksa apakah Anda memiliki cadangan data yang aktif di peranti Windows Anda, lihat [informasi Bantuan Microsoft: Pencadangan dan Pemulihan di Windows](https://support.microsoft.com/id-id/windows/pencadangan-dan-pemulihan-di-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef).
>
>  Windows menyertakan fitur ***Timeline***, yang dimaksudkan untuk meningkatkan produktivitas Anda dengan menyimpan catatan berkas yang Anda gunakan, situs yang Anda jelajahi, dan tindakan lain yang Anda lakukan di komputer. Jika Anda tidak bisa mengingat di mana Anda menyimpan suatu dokumen, Anda dapat mengeklik ikon *Timeline* di *taskbar* Windows 10 untuk melihat log visual yang diatur berdasarkan tanggal, dan Anda dapat kembali ke apa yang Anda butuhkan dengan mengeklik ikon pratinjau yang sesuai. Hal ini  membantu Anda menemukan berkas yang telah diganti namanya. Jika fitur *Timeline* diaktifkan, beberapa aktivitas komputer Anda — seperti berkas yang Anda sunting di Microsoft Office — juga dapat disinkronkan dengan peranti seluler Anda atau komputer lain yang Anda gunakan, sehingga Anda mungkin memiliki cadangan dari data Anda yang hilang di peranti lainnya. Baca selengkapnya tentang fitur *Timeline* dan cara menggunakannya di [panduan resmi](https://support.microsoft.com/id-id/windows/get-help-with-timeline-febc28db-034c-d2b0-3bbe-79aa0c501039).

Apakah Anda bisa menemukan data Anda atau memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#windows-restore)

### windows-restore

> Di beberapa kasus ada beberapa perangkat gratis dan sumber-terbuka yang dapat membantu menemukan konten yang hilang dan memulihkannya. Terkadang penggunaannya terbatas, dan sebagian besar perangkat yang terkenal berbayar.
>
> Sebagai contoh, Anda bisa mencoba [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (lihat [instruksi langkah-langkahnya](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/datarecoverywizard/free-data-recovery-software.htm), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/data-recovery-software.html), atau [Recuva](http://www.ccleaner.com/recuva).

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)


### linux-computer

> Beberapa rilisan Linux yang paling populer seperti Ubuntu memiliki perangkat pencadangan bawaan, misalnya [Déjà Dup](https://wiki.gnome.org/action/show/Apps/DejaDup) di Ubuntu. Jika ada pencadangan bawaan yang disertakan di sistem operasi yang Anda gunakan, Anda mungkin telah diingatkan untuk mengaktifkan pencadangan otomatis saat Anda pertama kali mulai menggunakan komputer Anda. Cari rilisan Linux Anda untuk melihat apakah alat pencadangan bawaan disertakan di dalamnya, dan seperti apa prosedur untuk memeriksa apakah perangkat tersebut sudah aktif serta bagaimana cara memulihkan data dari cadangan tersebut.
>
> Baca [Ubuntu & Deja Dup - Mulai Cadangkan](https://www.dedoimedo.com/computers/ubuntu-deja-dup.html) untuk memeriksa apakah Anda sudah mengaktifkan pencadangan otomatis di komputer Anda. Baca ["Pemulihan Seluruh Sistem dengan Déjà Dup"](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/Full) untuk petunjuk memulihkan data Anda yang hilang dari cadangan yang ada.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### mobile

Sistem operasi apa yang digunakan di ponsel Anda?

- [iOS](#ios-phone)
- [Android](#android-phone)


### ios-phone

> Anda mungkin telah mengaktifkan pencadangan otomatis dengan iCloud atau iTunes. Baca ["Pulihkan atau atur peranti Anda dari cadangan iCloud"](https://support.apple.com/kb/ph12521?locale=id) untuk memeriksa apakah Anda memiliki cadangan yang tersedia dan mempelajari bagaimana cara memulihkan data Anda.

Apakah Anda telah menemukan cadangan data Anda dan memulihkannya?

- [Ya](#resolved_end)
- [Tidak](#phone-which-data)

###  phone-which-data

Manakah dari berikut ini yang berlaku untuk data Anda yang hilang?

- [Data buatan-aplikasi, misalnya kontak, *feed*, dll.](#app-data-phone)
- [Data buatan-pengguna, misalnya foto, video, audio, catatan](#ugd-phone)

### app-data-phone

> Aplikasi seluler adalah aplikasi perangkat lunak yang didesain untuk dijalankan di peranti seluler Anda. Namun, sebagian besar aplikasi tersebut dapat diakses juga melalui *browser* di desktop. Jika Anda pernah mengalami kehilangan data buatan-aplikasi di ponsel Anda, coba akses aplikasi tersebut di *browser* desktop Anda, dengan cara masuk ke antarmuka situs web aplikasi tersebut dengan menggunakan kredensial Anda untuk aplikasi tersebut. Anda mungkin menemukan data yang hilang di antarmuka *browser* tersebut.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### ugd-phone

> Data buatan-pengguna adalah jenis data yang Anda buat atau hasilkan melalui aplikasi tertentu, dan jika terjadi kehilangan data, Anda perlu memeriksa apakah aplikasi tersebut memiliki pengaturan pencadangan bawaan yang diaktifkan atau memungkinkan cara untuk memulihkannya. Misalnya, jika Anda menggunakan WhatsApp di ponsel Anda dan percakapannya hilang atau terjadi suatu kesalahan, Anda dapat memulihkan percakapan Anda jika Anda telah mengaktifkan pengaturan pemulihan WhatsApp, atau jika Anda menggunakan suatu aplikasi untuk membuat dan menyimpan catatan dengan informasi sensitif atau bersifat pribadi, kadang mungkin aplikasi tersebut juga telah mengaktifkan opsi cadangan datanya tanpa Anda sadari.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#tech-assistance_end)

### android-phone

> Google memiliki layanan bawaan yang disertakan dalam Android, yaitu *Android Backup Service*. Secara bawaan, layanan ini membuat cadangan sejumlah tipe data dan mengasosiasikannya dengan layanan Google yang sesuai, di mana Anda juga bisa mengaksesnya melalui web. Di sebagian besar peranti Android, Anda dapat melihat pengaturan Sinkronisasi dengan membuka Pengaturan > Akun > Google, kemudian pilih alamat Gmail Anda. Jika Anda kehilangan data dari jenis yang Anda sinkronisasikan dengan akun Google Anda, Anda mungkin dapat memulihkannya dengan masuk ke akun Google Anda melalui *browser* web.

Apakah informasi ini membantu dalam memulihkan data Anda (sepenuhnya atau sebagian)?

- [Ya](#resolved_end)
- [Tidak](#phone-which-data)


### tech-assistance_end

> Jika kehilangan data Anda disebabkan oleh kerusakan fisik seperti peranti Anda jatuh ke lantai atau air, terkena pemadaman listrik atau masalah lainnya, situasi yang paling memungkinkan adalah Anda harus melakukan pemulihan data yang tersimpan di perangkat keras Anda. Jika Anda tidak tahu bagaimana cara melakukannya, Anda perlu menghubungi teknisi TI yang menguasai pemeliharaan perangkat keras dan peralatan elektronik yang dapat membantu Anda. Namun, tergantung konteks Anda dan sensitivitas data yang perlu Anda pulihkan, menghubungi toko TI mana saja tidaklah disarankan, lebih baik hubungi teknisi TI yang Anda kenal dan percaya.


### device-lost-theft_end

> Jika peranti Anda hilang, dicuri, atau disita, pastikan untuk mengubah semua kata sandi Anda sesegera mungkin dan mengunjungi sumber spesifik kami tentang [apa yang harus dilakukan jika peranti hilang](../../../lost-device).
>
> Jika Anda anggota masyarakat sipil, dan Anda membutuhkan bantuan untuk mendapatkan peranti baru untuk menggantikan yang hilang, Anda dapat menghubungi organisasi yang terdaftar di bawah ini.

:[](organisations?services=equipment_replacement)


### storage-devices_end

> Untuk mendapatkan kembali data yang telah hilang, ingatlah bahwa waktu adalah faktor yang penting. Sebagai contoh, memulihkan berkas yang tidak sengaja Anda hapus beberapa jam yang lalu atau sehari sebelumnya mungkin memiliki tingkat keberhasilan lebih tinggi dari berkas yang hilang sebulan sebelumnya.
>
> Pertimbangkan untuk menggunakan perangkat lunak untuk memulihkan data Anda yang hilang (akibat terhapus atau kerusakan) seperti [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) (untuk Windows, Mac, atau Linux - lihat [instruksi langkah-langkahnya](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)), [EaseUS Data Recovery Wizard](https://www.easeus.com/data-recovery-software/) (untuk Windows, Mac, Android, or iOS), [CleverFiles Disk Drill Data Recovery Software](https://www.cleverfiles.com/) (untuk Windows atau  Mac), atau [Recuva](http://www.ccleaner.com/recuva) (untuk Windows). Perhatikan bahwa alat tersebut tidak selalu berfungsi, karena sistem operasi Anda mungkin telah menimpa (*overwrite*) data baru di atas informasi yang telah Anda hapus. Oleh karena itu, Anda harus melakukan sesedikit mungkin aktivitas di komputer jika berkas Anda terhapus dan sedang mencoba mendapatkannya kembali dengan perangkat lunak tersebut di atas.
>
> Untuk meningkatkan peluang Anda memulihkan data, segera hentikan penggunaan peranti Anda. Penimpaan data baru terus-menerus pada diska keras dapat menurunkan peluang untuk menemukan dan mendapatkan kembali data tersebut.
>
> Jika opsi di atas tidak ada yang berhasil, hal yang paling memungkinkan adalah untuk melakukan pemulihan data yang disimpan di diska keras. Jika Anda tidak tahu bagaimana cara melakukannya, hubungi teknisi TI yang menguasai pemeliharaan perangkat keras dan peralatan elektronik yang dapat membantu Anda. Namun, tergantung konteks Anda dan sensitivitas data yang perlu Anda pulihkan, menghubungi toko TI mana saja tidaklah disarankan, lebih baik hubungi teknisi TI yang Anda kenal dan percaya.


### resolved_end

Kami harap panduan Pertolongan Pertama Darurat Digital ini bermanfaat. Silakan berikan masukan melalui [surel](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### final_tips

- Cadangan - selalu disarankan untuk memastikan Anda memiliki cadangan data - banyak cadangan yang berbeda, yang Anda simpan di tempat berbeda selain tempat yang sama dengan data Anda! Tergantung dari konteksnya, pilih untuk menyimpan cadangan data Anda di layanan *cloud* dan di peranti fisik eksternal yang tidak disambungkan ke komputer saat terhubung dengan internet.
- Untuk kedua tipe cadangan data, Anda harus melindungi data Anda dengan enkripsi. Lakukan pencadangan rutin dan bertahap untuk data terpenting Anda, periksa apakah Anda sudah menyiapkannya, dan uji apakah Anda dapat memulihkannya sebelum melakukan pembaruan perangkat lunak atau sistem operasi.
- Buat sistem folder yang terstruktur - Tiap orang memiliki caranya sendiri dalam mengorganisasi data dan informasi penting, tidak ada satu solusi yang cocok untuk semuanya. Meski demikian, penting halnya mempertimbangkan untuk membuat sistem folder yang sesuai dengan kebutuhan Anda. Dengan membuat sistem folder yang konsisten, Anda membuat hidup Anda lebih mudah, misalnya dengan lebih mengenal folder dan berkas mana yang harus lebih sering dibuat cadangannya, di mana letak informasi penting yang sedang Anda kerjakan, di mana Anda harus menyimpan data yang mengandung informasi pribadi dan sensitif tentang Anda dan kolaborator Anda, dan seterusnya. Seperti biasa, berhenti sejenak dan luangkan waktu untuk merencanakan tipe data yang Anda produksi atau kelola, dan pikirkan sistem folder yang dapat membuatnya lebih konsisten dan rapi.

#### Resources

- [Dokumentasi Komunitas Saluran Bantuan Access Now: Cadangkan dengan Aman (dalam bahasa Inggris)](https://communitydocs.accessnow.org/182-Secure_Backup.html)
- [Security in a Box: Taktik pencadangan (dalam bahasa Inggris)](https://securityinabox.org/id/files/backup/)
- [Halaman resmi tentang pencadangan Mac (dalam bahasa Inggris)](https://support.apple.com/mac-backup)
- [Cara mencadangkan data secara reguler di Windows](https://support.microsoft.com/id-id/windows/pencadangan-dan-pemulihan-di-windows-352091d2-bb9d-3ea3-ed18-52ef2b88cbef)
- [Cara mengaktifkan cadangan reguler di iPhone](https://support.apple.com/id-id/HT203977)
- [Cara mengaktifkan cadangan reguler di Android](https://support.google.com/android/answer/2819582?hl=id)
- [Cadangkan data Anda dengan aman (dalam bahasa Inggris)](https://johnopdenakker.com/securily-backup-your-data/)
- [Cara Mencadangkan Kehidupan Digital Anda (dalam bahasa Inggris)](https://www.wired.com/story/how-to-back-up-your-digital-life/)
- [Wikipedia - Pemulihan Data (dalam bahasa Inggris)](https://en.wikipedia.org/wiki/Data_recovery)
