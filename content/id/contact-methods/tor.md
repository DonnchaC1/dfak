---
layout: page
title: Tor
author: mfc
language: id
summary: Metode kontak
date: 2018-09
permalink: /id/contact-methods/tor.md
parent: /id/
published: true
---

Tor Browser adalah *browser* web yang berfokus pada privasi yang memungkinkan Anda berinteraksi dengan situs web secara anonim dengan tidak membagikan lokasi Anda (melalui alamat IP Anda) saat Anda mengakses situs web.

Sumber: [Tinjauan tentang Tor (dalam bahasa Inggris)](https://www.torproject.org/about/overview.html.en).