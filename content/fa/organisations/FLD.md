---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: اورژانس ۲۴/۷، جهانی; کار معمولی دوشنبه تا جمعه در ساعات اداری، IST (UTC+1)، کارکنان در مناطق زمانی مختلف در مناطق مختلف قرار دارند
response_time: همان روز یا روز بعد در مواقع اضطراری
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

فرانت لاین دیفندرز یک سازمان بین المللی مستقر در ایرلند است
که برای حمایت یکپارچه از مدافعان حقوق بشر در
خطر. فرانت لاین دیفندرز از طریق کمک هزینه های امنیتی، آموزش های امنیتی فیزیکی و دیجیتالی، حمایت و تبلیغات از مدافعان حقوق بشر در معرض خطر فوری پشتیبانی می کند.

فرانت لاین دیفندرز یک خط تلفن پشتیبانی اضطراری را به صورت 24/7 در اختیار دارد
+353-121-00489 برای مدافعان حقوق بشر در معرض خطر فوری به زبان عربی،
انگلیسی، فرانسوی، روسی یا اسپانیایی. هنگامی که مدافعان حقوق بشر با تهدید فوری جان خود مواجه می شوند، فرانت لاین دیفندرز می تواند کمک کند
جابجایی موقت Front Line Defenders آموزش هایی را در زمینه امنیت فیزیکی و امنیت دیجیتال ارائه می دهد. فرانت لاین دیفندرز همچنین موارد مدافعان حقوق بشر را در معرض خطر و کمپین ها و مدافعان در سطح بین المللی از جمله اتحادیه اروپا، سازمان ملل، سازوکارهای بین منطقه ای و مستقیماً با دولت ها برای حمایت از آنها، تبلیغ می کند.

