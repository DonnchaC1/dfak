﻿---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: ۲۴/۷، UTC+2
response_time: چهار ساعت
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

گرین هاست خدمات فناوری اطلاعات را با رویکردی اخلاقی و پایدار ارائه می‌دهد. خدمات ارائه شده آنها شامل میزبانی وب، سرویس‌های ابری و ارائه های قدرتمند در امنیت اطلاعات است. گرین هاست به طور فعال در توسعه منبع باز مشارکت دارد و در پروژه های مختلف در زمینه‌های فناوری، روزنامه‌نگاری، فرهنگ، آموزش، پایداری و آزادی اینترنت مشارکت دارد.


