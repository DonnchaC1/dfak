---
layout: page
title: "No puedo acceder a mi cuenta"
author: RaReNet
language: es
summary: "¿Tienes problemas para acceder a un correo electrónico, a una red social o a una cuenta web? ¿Alguna cuenta muestra actividad que no reconoces? Hay muchas cosas que puedes hacer para enfrentar este problema."
date: 2023-04
permalink: /es/topics/account-access-issues/
parent: /es/
---


# No puedo acceder a mi cuenta

Las redes sociales y medios de comunicación son ampliamente utilizados por componentes de la sociedad civil para comunicarse, compartir conocimientos y defender sus causas. Como consecuencia, dichas cuentas son fuertemente monitoreadas por actores maliciosos, que a menudo intentan comprometerlas, causando daños a los miembros de la sociedad civil y sus contactos.

Esta guía existe para ayudarte en caso de que hayas perdido el acceso a una de tus cuentas por haber sido comprometida.

Aquí dispones de un cuestionario para identificar la naturaleza de tu problema y encontrar posibles soluciones.

## Workflow

### Password-Typo

> Algunas veces, es posible que no puedas iniciar sesión en tus cuentas porque estás escribiendo la contraseña de forma errada, o porque la configuración de idioma del teclado no es la que usas habitualmente o
porque tienes activado el bloqueo de mayúsculas. Esto es más frecuente de lo que parece.
>
> Intenta escribir tu nombre de usuario y contraseña en un editor de texto para ver que sean correctos. Después cópialos del editor y pégalos en el formulario de inicio de sesión.

¿Las sugerencias anteriores te ayudaron a iniciar sesión en tu cuenta?

- [Sí](#resolved_end)
- [No](#account-disabled)

### account-disabled

> A veces, puede que no seas capaz de acceder a tu cuenta porque ha sido bloqueada o deshabilitada por la plataforma debido a violaciones de los Términos de Uso o reglas de la plataforma. Esto puede ocurrir cuando tu cuenta es reportada masivamente, o cuando los mecanismos de reporte o apoyo de la plataforma son abusados con la intención de censurar contenido en línea.
>
> Si tú estás viendo un mensaje de tu cuenta bloquedada, restingida, deshabilitada o suspendida, y tu crees que esto es un error, sigue cualquier mecanismo de reclamación que es proporcionado con el mensaje. Puedes encontrar información de como enviar reclamaciones en los siguientes links:
>
> - [Facebook](https://www.facebook.com/help/185747581553788?locale=es_ES&cms_id=185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856?locale=es_ES&cms_id=366993040048856&force_new_ighc=false)
> - [Twitter](https://help.twitter.com/es/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168?hl=es&sjid=9676536926565960948-EU)

¿La sugerencia anterior te ayudó a acceder a tu cuenta?

- [Sí](#resolved_end)
- [No](#what-type-of-account-or-service)

### what-type-of-account-or-service

¿A qué tipo de cuenta o servicio has perdido el acceso?

- [Facebook](#Facebook)
- [Página de Facebook](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#Protonmail)
- [Instagram](#Instagram)
- [TikTok](#Tiktok)
<!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

¿La página es administrada por otras personas?

- [Sí](#Other-admins-exist)
- [No](#Facebook-Page-recovery-form)

### Other-admins-exist

¿Las otras personas que administran la página tienen el mismo problema?

- [Sí](#Facebook-Page-recovery-form)
- [No](#Other-admin-can-help)

### Other-admin-can-help

> Por favor, pídele a las otras personas que administran la página que te agreguen nuevamente como administrador/a.

¿Esto solucionó el problema?

- [Sí](#Fb-Page_end)
- [No](#account_end)


### Facebook-Page-recovery-form

> Por favor, inicia sesión en Facebook y utiliza el [formulario de Facebook para recuperar la página](https://www.facebook.com/help/contact/164405897002583).  Si no puedes iniciar la sesión en tu cuenta Facebook, por favor, completa el [flujo de trabajo de recuperación de cuenta Facebook](#Facebook)
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

¿Tienes acceso al correo electrónico o teléfono móvil de recuperación asociado a la cuenta que quieres recuperar?

- [Sí](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> Comprueba en la bandeja de entrada de tu correo electrónico de recuperación si has recibido un correo electrónico de "Alerta de seguridad crítica para tu cuenta de Google vinculada" o SMS desde Google.
>
> Cuando revises por correos electrónicos, siempre ve con cautela de intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

¿Recibiste una "Alerta de seguridad crítica para tu cuenta de Google vinculada" o SMS desde Google?

- [Sí](#Email-received-google)
- [No](#Recovery-Form-google)

### Email-received-google

Una vez que ya has tenido verificada la legimitad del mensaje, revisa la información aportada en el correo electrónico. Comprueba si hay un enlace "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery-Link-Found-google)
- [No](#Recovery-Form-google)

### Recovery-Link-Found-google

> Por favor, utiliza el enlace "Recuperar tu cuenta" para recuperarla. Cuando sigues el enlace, revisa que la URL que estás visitando es de hecho una dirección "google.com".

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-google)

### Recovery-Form-google

> Intenta seguir las instrucciones en ["Cómo recuperar tu cuenta de Google o Gmail"](https://support.google.com/accounts/answer/7682439?hl=es).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

¿Tienes acceso al correo electrónico o al teléfono móvil de recuperación conectado?

- [Sí](#I-have-access-to-recovery-email-yahoo)
- [No](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

> Comprueba en la bandeja de entrada del correo electrónico de recuperación para ver si has recibido un correo electrónico de "Cambio de contraseña para tu cuenta de Yahoo" de Yahoo.
>
> Cuando revises por correos electrónicos, siempre ve con cautela de intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

¿Recibiste un correo electrónico de "Cambio de contraseña para tu cuenta de Yahoo" de Yahoo?

- [Sí](#Email-received-yahoo)
- [No](#Recovery-Form-Yahoo)

### Email-received-yahoo

Una vez has verificado la legitimidad del mensaje, revisa la información aportada en el correo electrónico. Por favor, verifica si hay un enlace "Recupera tu cuenta aquí". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Yahoo)
- [No](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Por favor, utiliza el enlace "Recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Sigue las instrucciones en ["Solucionar problemas al iniciar sesión en tu cuenta de Yahoo](https://es.ayuda.yahoo.com/kb/account/Solucionar-problemas-para-iniciar-sesi%C3%B3n-en-tu-cuenta-de-Yahoo-sln2051.html) para recuperar tu cuenta.
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Twitter)
- [No](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

> Verifica en la bandeja de entrada del email de recuperación para ver si recibiste un correo electrónico con asunto "Tu contraseña de Twitter ha sido cambiada" de Twitter. ¿Lo recibiste?
>
> Cuando revises por correos electrónicos, siempre ve con cautela de intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

Recibiste un correo electrónico con asunto "Tu contraseña de Twitter ha sido cambiada" de Twitter?

- [Sí](#Email-received-Twitter)
- [No](#Recovery-Form-Twitter)

### Email-received-Twitter

Una vez has verificado la legitimidad del mensaje, revisa la información aportada en el correo electrónico. Por favor, comprueba si el mensaje contiene un enlace de "recuperar tu cuenta". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Twitter)
- [No](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Utiliza el enlace "recuperar tu cuenta" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Si piensas que tu cuenta Twitter ha sido comprometida, intenta seguir los pasos en [Ayuda mi cuenta comprometida](https://help.twitter.com/en/safety-and-security/twitter-account-compromised).
>
> Si tu cuenta no está comprometida, o tienes otros problemas de acceso, puedes seguir los siguientes pasos en ["Pide ayuda restaurando tu cuenta"](https://help.twitter.com/es/forms/account-access/reactivate-my-account).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)


<!---=========================================================
//ProtonmailProtonmailProtonmailProtonmailProtonmailProtonmailProtonmail
//========================================================= -->

### Protonmail

> Por favor, ten en cuenta que si has restablecido tu contraseña, no serás capaz de leer tus emails y contactos existentes, ya que están cifrados con tu contraseña y llave de cifrado. Datos antiguos pueden ser recuperados si tienes acceso a un fichero o una frase de recuperación siguiendo los pasos en [Recuperar Mensajes y Ficheros Cifrados (en inglés)](https://proton.me/support/recover-encrypted-messages-files).
>
> Sigue las [instrucciones para restablecer tu contraseña (en inglés)](https://protonmail.com/support/knowledge-base/reset-password/)
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

¿Tienes acceso al correo electrónico o teléfono móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Hotmail)
- [No](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

> Comprueba si has recibido un correo electrónico de "Cambio de contraseña de cuenta de Microsoft" enviado por Microsoft.
>
> Cuando revises por correos electrónicos, siempre ve con cautela de intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

¿Recibiste un correo electrónico de "Cambio de contraseña de cuenta de Microsoft" enviado por Microsoft?

- [Sí](#Email-received-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Una vez ya has verificaddo la legitimidad del mensaje, revisa la información aportada por el email. Por favor, comprueba si el mensaje contiene el enlace "Restablecer tu contraseña". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Hotmail)
- [No](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Utiliza el enlace "Restablecer tu contraseña" para establecer una nueva contraseña y recuperar tu cuenta.

¿Pudiste recuperar tu cuenta con el enlace "Restablecer tu contraseña"?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Por favor, prueba [el "Ayudante de inicio de sesión de cuenta Microsoft"](https://support.microsoft.com/es-ES/home/contact?linkquery=Help%20me%20sign%20in%20to%20my%20Microsoft%20account). Sigue las instrucciones de esta herramienta, incluyendo añadir la cuenta que estás intentando recuperar y respondiendo a las preguntas sobre la información disponible a recuperar.
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes vía formularios web. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!---==================================================================
//FacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebook
//================================================================== -->


### Facebook

¿Tienes acceso al correo electrónico / móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Facebook)
- [No](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

> Comprueba si recibiste un correo electrónico con "¿Acabas de restablecer tu contraseña?" de Facebook.
>
> Cuando revises por correos electrónicos, siempre ve con cautela de intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

¿Recibiste un correo electrónico con "¿Acabas de restablecer tu contraseña?" de Facebook?

- [Sí](#Email-received-Facebook)
- [No](#Recovery-Form-Facebook)

### Email-received-Facebook

Una vez has verificado la legitimidad del mensaje, revisa la información aportada en el correo electrónico. ¿El correo electrónico contiene un mensaje que dice "No he sido yo" con un enlace?

- [Sí](#Recovery-Link-Found-Facebook)
- [No](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Utiliza el enlace "No he sido yo" en el mensaje para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta haciendo clic en el enlace?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Intenta con [el formulario para recuperar tu cuenta](https://es-la.facebook.com/login/identify).
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes vía formularios web. Guarda esta página en tus marcadores y vuelve a esta página en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

¿Tienes acceso al correo electrónico o teléfono móvil de recuperación asociado?

- [Sí](#I-have-access-to-recovery-email-Instagram)
- [No](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

> Comprueba si has recibido un correo electrónico con "Tu contraseña de Instagram ha sido cambiada" de Instagram.
>
> Cuando revises por correos electrónicos, siempre ve con cautela ante intentos de phishing (fraude electrónico). Si no tienes seguridad de la legitimidad de un mensaje, por favor, revisa el [Flujo de trabajo de Mensajes Sospechosos](../../../suspicious-messages/).

¿Recibiste un correo electrónico con "Tu contraseña de Instagram ha sido cambiada" de Facebook?

- [Sí](#Email-received-Instagram)
- [No](#Recovery-Form-Instagram)

### Email-received-Instagram

Una vez has verificado la legitimad del mensaje, revisa la información aportada por el email. Por favor, comprueba si hay un enlace con "asegura tu cuenta aquí". ¿Está ahí?

- [Sí](#Recovery-Link-Found-Instagram)
- [No](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Por favor, utiliza el enlace "asegura tu cuenta aquí" para recuperar tu cuenta.

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Intenta seguir las instrucciones en ["Creo que mi cuenta de Instagram ha sido pirateada"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked) para recuperar tu cuenta.
>
> Ten en cuenta que puede pasar un tiempo antes de recibir una respuesta a tus solicitudes vía formularios web. Guarda esta página en tus marcadores y vuelve a visitarla en unos días.

¿Ha funcionado el procedimiento de recuperación?

- [Sí](#resolved_end)
- [No](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

¿Tienes acceso al conectado email/móvil de recuperación?

- [Sí](#I-have-access-to-recovery-email-Tiktok)
- [No](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

> Si tienes acceso al email de recuperación, por favor, intenta restaurar tu contraseña siguiendo el [Restablecer contraseña de Tiktok](https://support.tiktok.com/es/log-in-troubleshoot/log-in/reset-password).

¿Pudiste recuperar tu cuenta?

- [Sí](#resolved_end)
- [No](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

> Por favor, intenta seguir las instrucciones en ["Mi cuenta ha sido pirateada"](https://support.tiktok.com/es/log-in-troubleshoot/log-in/my-account-has-been-hacked) para recuperar tu cuenta.

¿El procedimiento de recuperación funcionó?

- [Sí](#resolved_end)
- [No](#account_end)


### Fb-Page_end

¡Nos alegra que tu problema esté resuelto! Lee estas recomendaciones para ayudarte a minimizar la posibilidad de perder el acceso a tu página en el futuro:

- Activa la autenticación de dos factores (o 2FA) para todas personas que administran la página.
- Asigna roles de administrador solo a las personas en las que confías y que sean rápidas ante incidentes.
- Si hay alguien en quien confías, considera el tener más de una cuenta de administración. Ten presente que deberías activar 2FA para todas las cuentas administrativas.
- Regularmente revisa privilegios y permisos en la página. Siempre asigna el nivel mínimo de privilegio necesario para que la persona que la utiliza pueda llevar a cabo su trabajo.

### account_end

Si los procedimientos sugeridos en este flujo de trabajo no te han ayudado a recuperar el acceso a tu cuenta, puedes comunicarte con las siguientes organizaciones para solicitar más ayuda:

:[](organisations?services=account)

### resolved_end

Con suerte, este Kit de Primeros Auxilios Digitales te ha sido útil. Por favor danos tu opinión [vía email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Lee estas recomendaciones para ayudar a minimizar la posibilidad de perder el acceso a tus cuentas en el futuro.

- Siempre es una buena idea activar la autenticación de dos factores (2FA) para las cuentas en todas las plataformas que lo permitan.
- Nunca uses la misma contraseña para más de una cuenta. Si lo haces, cámbialas lo antes posible.
- El uso de un administrador de contraseñas te ayudará a crear y recordar contraseñas únicas y seguras para todas tus cuentas.
- Ten cuidado al usar redes wifi públicas abiertas que no sean de confianza y, cuando sea posible, conéctate a ellas a través de un red privada virtual (VPN) o Tor.

#### resources

- [[Documentación para la comunidad de la línea de ayuda en seguridad digital de Access Now: Recomendaciones para administradores de contraseñas para equipos (en inglés](https://communitydocs.accessnow.org/295-Password_managers.html)
- [Surveillance Self-Defense: cómo protegerse en las redes sociales](https://ssd.eff.org/es/module/protegi%C3%A9ndose-en-las-redes-sociales)
- [Surveillance Self-Defense: Creación de contraseñas seguras mediante administradores de contraseñas](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras#0)
